/* global angular */

var app = angular.module('tripApp', []);

app.controller('appCtrl', function ($scope, $compile) {

    $scope.responseMsg = "";
    currentPage = 1;
    $scope.people = [];
    $scope.drivers = [];

 $scope.getDetail = function (item) {
       
        $("#modalDiv").load("api/get_driver_detail.php?id="+item.id, null, function () {
            $compile($("#detailModal"))($scope);
            $("#detailModal").modal();
            $scope.$apply();
        });
    };
    
    $scope.showRatingModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = {"driver_id": item.id};
        $scope.update = false;
        $("#upsertRatingModal").modal();
    };

    $scope.getPeople = function (page) {
        $.ajax({
            url: "api/get_people.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.people = $scope.people.concat(result.data);
                if ($scope.people.length < result.total) {
                    $scope.getPeople(page + 1);
                }
                $scope.$apply();
            }
        });
    };

    $scope.getDrivers = function (page) {
        $.ajax({
            url: "api/get_drivers.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.drivers = $scope.drivers.concat(result.data);
                if ($scope.drivers.length < result.total) {
                    $scope.getDrivers(page + 1);
                }
                $scope.$apply();
            }
        });
    };

    $scope.getDrivers(1);
    $scope.getPeople(1);

    $scope.showUpsertModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = item;
        $scope.update = false;
        if (item !== null) {
            $scope.update = true;
            if (item.licence_date && typeof item.licence_date === 'string') {
                var dateParts = item.licence_date.split("-");
                var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0, 2));
                $scope.selectedItem.licence_date = date;
            }

            if (item.licence_expiry_date && typeof item.licence_expiry_date === 'string') {
                var dateParts = item.licence_expiry_date.split("-");
                var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0, 2));
                $scope.selectedItem.licence_expiry_date = date;
            }
        }

        $("#upsertModal").modal();
    };

    $scope.showDeleteModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = item;
        if (item.licence_date && typeof item.licence_date === 'string') {
            var dateParts = item.licence_date.split("-");
            var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0, 2));
            $scope.selectedItem.licence_date = date;
        }

        if (item.licence_expiry_date && typeof item.licence_expiry_date === 'string') {
            var dateParts = item.licence_expiry_date.split("-");
            var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0, 2));
            $scope.selectedItem.licence_expiry_date = date;
        }
        $("#deleteModal").modal();
    };

    $scope.getNext = function () {
        currentPage = currentPage + 1;
        $('#searchForm').submit();
    };

    $scope.getPrevious = function () {
        currentPage = currentPage - 1;
        $('#searchForm').submit();
    };
});

$(function () {
    var scope = angular.element($("#page-top")).scope();
    $('#upsertForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/upsert_driver.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result);
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });

    $('#searchForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        $.ajax({
            url: "api/get_drivers.php?page=" + currentPage,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                scope.result = JSON.parse(result);
                currentPage = scope.result.currentPage;
                scope.processing = false;
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                currentPage = scope.result.currentPage;
                scope.$apply();
            }
        });
    });

    $('#deleteForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/delete_driver.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result)
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });

    $('#upsertRatingForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/upsert_driver_rating.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result);
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });
    $('#searchForm').submit();
});

