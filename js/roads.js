/* global angular */

var app = angular.module('tripApp', []);

app.controller('appCtrl', function ($scope,$compile) {

    $scope.responseMsg = "";
    currentPage = 1;

$scope.people = [];
    $scope.roads = [];

$scope.getDetail = function (item) {
       
        $("#modalDiv").load("api/get_road_detail.php?id="+item.id, null, function () {
            $compile($("#detailModal"))($scope);
            $("#detailModal").modal();
            $scope.$apply();
        });
    };
    
    $scope.getPeople = function (page) {
        $.ajax({
            url: "api/get_people.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.people = $scope.people.concat(result.data);
                if ($scope.people.length < result.total) {
                    $scope.getPeople(page + 1);
                }
                $scope.$apply();
            }
        });
    };


    $scope.getRoads = function (page) {
        $.ajax({
            url: "api/get_roads.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.roads = $scope.roads.concat(result.data);
                if ($scope.roads.length < result.total) {
                    $scope.getRoads(page + 1);
                }
                $scope.$apply();
            }
        });
    };

    $scope.getRoads(1);
    $scope.getPeople(1);
    
      $scope.showRatingModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = {"road_id": item.id};
        $scope.update = false;
        $("#upsertRatingModal").modal();
    };
    
    $scope.showUpsertModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = item;
        $scope.update = false;
        if (item !== null) {
            $scope.update = true;
            if (item.birth_date && typeof item.birth_date === 'string') {
                var dobParts = item.birth_date.split("-");
                var dob = new Date(dobParts[0], dobParts[1] - 1, dobParts[2].substr(0, 2));
                $scope.selectedItem.birth_date = dob;
            }
        }

        $("#upsertModal").modal();
    };

    $scope.showDeleteModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = item;
        if (item.birth_date && typeof item.birth_date === 'string') {
            var dobParts = item.birth_date.split("-");
            var dob = new Date(dobParts[0], dobParts[1] - 1, dobParts[2].substr(0, 2));
            $scope.selectedItem.birth_date = dob;
        }
        $("#deleteModal").modal();
    };
    
    $scope.getNext = function () {
        currentPage = currentPage+1;
        $('#searchForm').submit();
    };
    
     $scope.getPrevious = function () {
        currentPage = currentPage-1;
        $('#searchForm').submit();
    };
});

$(function () {
    var scope = angular.element($("#page-top")).scope();
    $('#upsertForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/upsert_road.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result)
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });

    $('#searchForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        $.ajax({
            url: "api/get_roads.php?page="+currentPage,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                scope.result = JSON.parse(result);
                currentPage = scope.result.currentPage;
                scope.processing = false;
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                currentPage = scope.result.currentPage;
                scope.$apply();
            }
        });
    });

    $('#deleteForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/delete_road.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result)
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });
 $('#upsertRatingForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/upsert_road_rating.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result);
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });
    $('#searchForm').submit();
});

