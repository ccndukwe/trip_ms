/* global angular */

var app = angular.module('tripApp', []);

app.controller('appCtrl', function ($scope, $compile) {

    $scope.responseMsg = "";
    currentPage = 1;
    $scope.drivers = [];
    $scope.vehicles = [];

 $scope.getDetail = function (item) {
       
        $("#modalDiv").load("api/get_trip_detail.php?id="+item.id, null, function () {
            $compile($("#detailModal"))($scope);
            $("#detailModal").modal();
            $scope.$apply();
        });
    };
    $scope.getDrivers = function (page) {
        $.ajax({
            url: "api/get_drivers.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.drivers = $scope.drivers.concat(result.data);
                if ($scope.drivers.length < result.total) {
                    $scope.getDrivers(page + 1);
                }
                $scope.$apply();
            }
        });
    };

    $scope.getVehicles = function (page) {
        $.ajax({
            url: "api/get_vehicles.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.vehicles = $scope.vehicles.concat(result.data);
                if ($scope.vehicles.length < result.total) {
                    $scope.getVehicles(page + 1);
                }
                $scope.$apply();
            }
        });
    };

    $scope.getVehicles(1);
    $scope.getDrivers(1);

    $scope.showUpsertModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = item;
        $scope.update = false;
        if (item !== null) {
            $scope.update = true;
            if (item.start_time && typeof item.start_time === 'string') {
                var datetime = item.start_time.replace(" ", "T") + "Z";
                console.log(datetime);
                $scope.selectedItem.start_time = new Date(datetime);
            }
            if (item.end_time && typeof item.end_time === 'string') {
                var datetime = item.end_time.replace(" ", "T") + "Z";
                $scope.selectedItem.end_time = new Date(datetime);
            }
        }

        $("#upsertModal").modal();
    };

    $scope.showDeleteModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = item;

        if (item.start_time && typeof item.start_time === 'string') {
            var datetime = item.start_time.replace(" ", "T") + "Z";
            console.log(datetime);
            $scope.selectedItem.start_time = new Date(datetime);
        }
        if (item.end_time && typeof item.end_time === 'string') {
            var datetime = item.end_time.replace(" ", "T") + "Z";
            $scope.selectedItem.end_time = new Date(datetime);
        }
        $("#deleteModal").modal();
    };

    $scope.getNext = function () {
        currentPage = currentPage + 1;
        $('#searchForm').submit();
    };

    $scope.getPrevious = function () {
        currentPage = currentPage - 1;
        $('#searchForm').submit();
    };
});

$(function () {
    var scope = angular.element($("#page-top")).scope();
    $('#upsertForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/upsert_trip.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result);
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });

    $('#searchForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        $.ajax({
            url: "api/get_trips.php?page=" + currentPage,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                scope.result = JSON.parse(result);
                currentPage = scope.result.currentPage;
                scope.processing = false;
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                currentPage = scope.result.currentPage;
                scope.$apply();
            }
        });
    });

    $('#deleteForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/delete_trip.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result)
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });

    $('#searchForm').submit();
});

