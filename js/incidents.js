/* global angular */

var app = angular.module('tripApp', []);

app.controller('appCtrl', function ($scope) {

    $scope.responseMsg = "";
    currentPage = 1;
    $scope.trips = [];
    $scope.incident_types = [];
    $scope.roads = [];

    $scope.getRoads = function (page) {
        $.ajax({
            url: "api/get_roads.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.roads = $scope.roads.concat(result.data);
                if ($scope.roads.length < result.total) {
                    $scope.getDrivers(page + 1);
                }
                $scope.$apply();
            }
        });
    };


    $scope.getTrips = function (page) {
        $.ajax({
            url: "api/get_trips.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.trips = $scope.trips.concat(result.data);
                if ($scope.trips.length < result.total) {
                    $scope.getTrips(page + 1);
                }
                $scope.$apply();
            }
        });
    };

    $scope.getIncidentTypes = function (page) {
        $.ajax({
            url: "api/get_incident_types.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.incident_types = $scope.incident_types.concat(result.data);
                if ($scope.incident_types.length < result.total) {
                    $scope.getIncidentTypes(page + 1);
                }
                $scope.$apply();
            }
        });
    };

    $scope.getRoads(1);
    $scope.getTrips(1);
    $scope.getIncidentTypes(1);

    $scope.showUpsertModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = item;
        $scope.update = false;
        if (item !== null) {
            $scope.update = true;
            if (item.incident_time && typeof item.incident_time === 'string') {
                var datetime = item.incident_time.replace(" ", "T") + "Z";
                $scope.selectedItem.incident_time = new Date(datetime);
            }
        }

        $("#upsertModal").modal();
    };

    $scope.showDeleteModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = item;

        if (item.incident_time && typeof item.incident_time === 'string') {
            var datetime = item.incident_time.replace(" ", "T") + "Z";
            $scope.selectedItem.incident_time = new Date(datetime);
        }
        $("#deleteModal").modal();
    };

    $scope.getNext = function () {
        currentPage = currentPage + 1;
        $('#searchForm').submit();
    };

    $scope.getPrevious = function () {
        currentPage = currentPage - 1;
        $('#searchForm').submit();
    };
});

$(function () {
    var scope = angular.element($("#page-top")).scope();
    $('#upsertForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/upsert_incident.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result);
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });

    $('#searchForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        $.ajax({
            url: "api/get_incidents.php?page=" + currentPage,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                scope.result = JSON.parse(result);
                currentPage = scope.result.currentPage;
                scope.processing = false;
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                currentPage = scope.result.currentPage;
                scope.$apply();
            }
        });
    });

    $('#deleteForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/delete_incident.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result)
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });

    $('#searchForm').submit();
});

