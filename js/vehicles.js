/* global angular */

var app = angular.module('tripApp', []);

app.controller('appCtrl', function ($scope, $compile) {

    $scope.responseMsg = "";
    currentPage = 1;
    $scope.people = [];
     $scope.vehicles = [];


$scope.getDetail = function (item) {
       
        $("#modalDiv").load("api/get_vehicle_detail.php?id="+item.id, null, function () {
            $compile($("#detailModal"))($scope);
            $("#detailModal").modal();
            $scope.$apply();
        });
    };
    
    $scope.getPeople = function (page) {
        $.ajax({
            url: "api/get_people.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.people = $scope.people.concat(result.data);
                if ($scope.people.length < result.total) {
                    $scope.getPeople(page + 1);
                }
                $scope.$apply();
            }
        });
    };

 $scope.getVehicles = function (page) {
        $.ajax({
            url: "api/get_vehicles.php?page=" + page + "&size=100",
            type: "POST",
            success: function (result) {
                result = JSON.parse(result);
                $scope.vehicles = $scope.vehicles.concat(result.data);
                if ($scope.vehicles.length < result.total) {
                    $scope.getVehicles(page + 1);
                }
                $scope.$apply();
            }
        });
    };

    $scope.getVehicles(1);
    $scope.getPeople(1);

    $scope.showRatingModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = {"vehicle_id": item.id};
        $scope.update = false;
        $("#upsertRatingModal").modal();
    };

    $scope.showUpsertModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = item;
        $scope.update = false;
        if (item !== null) {
            $scope.update = true;
            if (item.registration_date && typeof item.registration_date === 'string') {
                var dateParts = item.registration_date.split("-");
                var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0, 2));
                $scope.selectedItem.registration_date = date;
            }
        }

        $("#upsertModal").modal();
    };

    $scope.showDeleteModal = function (item) {
        $scope.processing = false;
        $scope.responseMsg = "";
        $scope.selectedItem = item;
        if (item.registration_date && typeof item.registration_date === 'string') {
            var dateParts = item.registration_date.split("-");
            var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0, 2));
            $scope.selectedItem.registration_date = date;
        }
        $("#deleteModal").modal();
    };

    $scope.getNext = function () {
        currentPage = currentPage + 1;
        $('#searchForm').submit();
    };

    $scope.getPrevious = function () {
        currentPage = currentPage - 1;
        $('#searchForm').submit();
    };
});

$(function () {
    var scope = angular.element($("#page-top")).scope();
    $('#upsertForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/upsert_vehicle.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result);
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });

    $('#searchForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        $.ajax({
            url: "api/get_vehicles.php?page=" + currentPage,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                scope.result = JSON.parse(result);
                currentPage = scope.result.currentPage;
                scope.processing = false;
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                currentPage = scope.result.currentPage;
                scope.$apply();
            }
        });
    });

    $('#deleteForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/delete_vehicle.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result)
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });

    $('#upsertRatingForm').submit(function (e) {
        e.preventDefault();
        scope.processing = true;
        scope.responseMsg = "";
        $.ajax({
            url: "api/upsert_vehicle_rating.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (result) {
                result = JSON.parse(result);
                scope.processing = false;
                scope.responseMsg = result.message;
                if (result.success) {
                    $('#searchForm').submit();
                }
                scope.$apply();
            },
            error: function (request, status, error) {
                scope.processing = false;
                scope.$apply();
            }
        });
    });

    $('#searchForm').submit();
});

