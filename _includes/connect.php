 <?php
require 'utils.php'; // File used to store function for performing common php tasks.

$servername = "localhost:3306";
$username = "root";
$password = "";
$db = "mait_trip_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error); // return an error if connection fails
} 