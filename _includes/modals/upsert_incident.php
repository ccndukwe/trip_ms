<div class="modal fade" id="upsertModal" tabindex="-1" role="dialog" aria-labelledby="upsertModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upsertModalTitle">

                    {{update?"Update "+selectedItem.name:"Add New Incident"}}
                    <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" name="upsertForm" id="upsertForm">
                <div class="modal-body">
                    <p class="text-warning font-weight-bold text-center">{{responseMsg}}</p>
                    <input ng-value="selectedItem.id" type="hidden" name="selected_id">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="roads">Road</label>
                            <select id="roads" required name="road_id" class="form-control" ng-model="selectedItem.road_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in roads" ng-value="{{item.id}}">
                                    {{item.name}}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="trips">Trip</label>
                            <select id="trips" required name="trip_id" class="form-control" ng-model="selectedItem.trip_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in trips" ng-value="{{item.id}}">
                                    {{item.registration_number+" : "+item.start_time +" : "+item.start_location}}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                             <label for="incident_types">Incident Type</label>
                            <select id="incident_types" required name="incident_type_id" class="form-control" ng-model="selectedItem.incident_type_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in incident_types" ng-value="{{item.id}}">
                                    {{item.name}}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="incident_time">Incident Time</label>
                            <input ng-model="selectedItem.incident_time"   required=""
                                   type="datetime-local" name="incident_time" class="form-control" 
                                   id="incident_time" placeholder="Incident Time">
                        </div>
                       
                    </div>
                   
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="remark">Remark</label>
                            <textarea ng-model="selectedItem.remark" rows="3"
                                      type="text" name="remark" class="form-control" id="distance" placeholder="remark"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" ng-disabled="processing" 
                            class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </form>
        </div>
    </div>
</div>