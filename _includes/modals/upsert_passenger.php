<div class="modal fade" id="upsertModal" tabindex="-1" role="dialog" aria-labelledby="upsertModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upsertModalTitle">

                    {{update?"Update "+selectedItem.first_name + " " + selectedItem.last_name:"Add New Passenger"}}
                    <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" name="upsertForm" id="upsertForm">
                <div class="modal-body">
                    <p class="text-warning font-weight-bold text-center">{{responseMsg}}</p>
                    <input ng-value="selectedItem.id" type="hidden" name="selected_id">
                    <div class="form-row">

                        <div class="form-group col-md-12">
                            <label for="inputOwner">Trip</label>
                            <select id="inputOwner" required name="trip_id" class="form-control" ng-model="selectedItem.trip_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in trips" ng-value="{{item.id}}">
                                    {{item.registration_number+" : "+item.start_time +" : "+item.start_location}}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputDriver">Person</label>
                            <select id="inputDriver" required name="person_id" class="form-control" ng-model="selectedItem.person_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in people" ng-value="{{item.id}}">
                                    {{item.first_name + " " + item.last_name}}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="seat_number">Seat Number</label>
                            <input ng-model="selectedItem.seat_number"   required
                                   type="text" name="seat_number" class="form-control" id="end_location" placeholder="Seat Number">
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" ng-disabled="processing" 
                                class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                    </div>
            </form>
        </div>
    </div>
</div>
</div>