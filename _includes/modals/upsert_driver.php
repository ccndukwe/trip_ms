<div class="modal fade" id="upsertModal" tabindex="-1" role="dialog" aria-labelledby="upsertModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upsertModalTitle">

                    {{update?"Update "+selectedItem.make+" - "+selectedItem.model:"Add New Driver"}}
                    <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" name="upsertForm" id="upsertForm">
                <div class="modal-body">
                    <p class="text-warning font-weight-bold text-center">{{responseMsg}}</p>
                    <input ng-value="selectedItem.id" type="hidden" name="selected_id">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputOwner">Person</label>
                            <select id="inputOwner" required name="person_id" class="form-control" ng-model="selectedItem.person_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in people" ng-value="{{item.id}}">
                                    {{item.first_name+" "+item.last_name}}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputmake">License Id</label>
                            <input ng-model="selectedItem.licence_id"   required
                                   type="text" name="licence_id" class="form-control" id="inputmake" placeholder="License Id">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputcategory">License Class</label>
                             <select id="inputcategory"  ng-model="selectedItem.licence_class" name="licence_class" class="form-control m-1"> 
                                                <option value="" selected>Choose...</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                                <option value="D">D</option>
                                                <option value="E">E</option>
                                                <option value="F">F</option>
                                            </select>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="licence_date1">License date</label>
                            <input ng-model="selectedItem.licence_date"  
                                   type="date" name="licence_date" class="form-control" id="licence_date1" placeholder="License date">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="licence_expiry_date1">License expire date</label>
                            <input type="date" ng-model="selectedItem.licence_expiry_date"  name="licence_expiry_date" 
                                   class="form-control" id="licence_expiry_date1" placeholder="License expire date">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" ng-disabled="processing" 
                            class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </form>
        </div>
    </div>
</div>