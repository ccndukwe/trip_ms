<div class="modal fade" id="upsertModal" tabindex="-1" role="dialog" aria-labelledby="upsertModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upsertModalTitle">

                    {{update?"Update "+selectedItem.name:"Add New Trip"}}
                    <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" name="upsertForm" id="upsertForm">
                <div class="modal-body">
                    <p class="text-warning font-weight-bold text-center">{{responseMsg}}</p>
                    <input ng-value="selectedItem.id" type="hidden" name="selected_id">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputDriver">Vehicle</label>
                            <select id="inputDriver" required name="vehicle_id" class="form-control" ng-model="selectedItem.vehicle_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in vehicles" ng-value="{{item.id}}">
                                    {{item.make+" : "+item.model+" : "+item.year}}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputOwner">Driver</label>
                            <select id="inputOwner" required name="driver_id" class="form-control" ng-model="selectedItem.driver_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in drivers" ng-value="{{item.id}}">
                                    {{item.first_name+" "+item.last_name}}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="start_location">Start Location</label>
                            <input ng-model="selectedItem.start_location"   required
                                   type="text" name="start_location" class="form-control" id="start_location" placeholder="Start Location">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="end_location">End Location</label>
                            <input ng-model="selectedItem.end_location" 
                                   type="text" name="end_location" class="form-control" id="end_location" placeholder="End Location">
                        </div>
                       
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="start_time">Start Time</label>
                            <input ng-model="selectedItem.start_time"   required=""
                                   type="datetime-local" name="start_time" class="form-control" id="start_time" placeholder="Start Time">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="end_time">End Time</label>
                            <input type="datetime-local" ng-model="selectedItem.end_time"  name="end_time" 
                                   class="form-control" id="end_time" placeholder="End time">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="max_speed">Max Speed</label>
                            <input ng-model="selectedItem.max_speed" 
                                   type="number" name="max_speed" class="form-control" id="max_speed" placeholder="Max Speed">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="average_speed">Average Speed</label>
                            <input ng-model="selectedItem.average_speed"  
                                   type="number" name="average_speed" class="form-control" id="average_speed" placeholder="Average Speed">
                        </div>
                       
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="distance_covered">Distance Covered</label>
                            <input ng-model="selectedItem.distance_covered"  
                                   type="number" name="distance_covered" class="form-control" id="distance_covered" placeholder="Distance Covered">
                        </div>
                        <div class="form-group col-md-6">
                            <span ></span>
                            <label for="route_map">Map
                                <a target="_blank" ng-if="selectedItem.route_map" 
                                   href="uploads/{{selectedItem.route_map}}">View current Map</a></label>
                            <input type="file" name="route_map"  class="form-control" id="route_map">
                        </div>
                       
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="remark">Remark</label>
                            <textarea ng-model="selectedItem.remark" rows="3"
                                      type="text" name="remark" class="form-control" id="distance" placeholder="remark"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" ng-disabled="processing" 
                            class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </form>
        </div>
    </div>
</div>