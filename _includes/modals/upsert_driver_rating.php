<div class="modal fade" id="upsertRatingModal" tabindex="-1" role="dialog" aria-labelledby="upsertModalRatingTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upsertModalRatingTitle">

                    {{update?"Update Rating":"Add New Rating"}}
                    <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" name="upsertRatingForm" id="upsertRatingForm">
                <div class="modal-body">
                    <p class="text-warning font-weight-bold text-center">{{responseMsg}}</p>
                    <input ng-value="selectedItem.id" type="hidden" name="selected_id">
                    <div class="form-row">
                       <div class="form-group col-md-12">
                            <label for="inputOwner111">Driver</label>
                            <select id="inputOwner111" required name="driver_id" class="form-control" ng-model="selectedItem.driver_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in drivers" ng-value="{{item.id}}">
                                    {{item.first_name+" "+item.last_name}}
                                </option>
                            </select>
                        </div>
                        </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="rating">Rating</label>
                            <select id="rating" required name="rating" class="form-control" ng-model="selectedItem.rating"> 
                                <option value="" selected>Choose...</option>
                                <option ng-value="1">1</option>
                                <option ng-value="2">2</option>
                                <option ng-value="3">3</option>
                                <option ng-value="4">4</option>
                                <option ng-value="5">5</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputpeople111">Person</label>
                            <select id="inputpeople111" required name="person_id" class="form-control" ng-model="selectedItem.person_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in people" ng-value="{{item.id}}">
                                    {{item.first_name + " " + item.last_name}}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="remark111">Comment</label>
                            <textarea ng-model="selectedItem.rating_comment" rows="3"
                                      type="text" name="comment" class="form-control" id="remark111" placeholder="Comment"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" ng-disabled="processing" 
                            class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </form>
        </div>
    </div>
</div>