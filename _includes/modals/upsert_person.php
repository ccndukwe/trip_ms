<!-- Add Person Modal -->
<div class="modal fade" id="upsertModal" tabindex="-1" role="dialog" aria-labelledby="upsertModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upsertModalTitle">
                    
                    {{update?"Update "+selectedItem.first_name+" "+selectedItem.last_name:"Add New Person"}}
                <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" name="upsertForm" id="upsertForm">
                <div class="modal-body">
                    <p class="text-warning font-weight-bold text-center">{{responseMsg}}</p>
                    <input ng-value="selectedItem.id" type="hidden" name="selected_id">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="first_name">First Name</label>
                            <input ng-model="selectedItem.first_name" type="text"  required
                                   name="first_name" class="form-control" id="first_name" placeholder="Firstname">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="last_name">Last Name</label>
                            <input ng-model="selectedItem.last_name"  required
                                   type="text" name="last_name" class="form-control" id="last_name" placeholder="Lastname">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Email</label>
                            <input ng-model="selectedItem.email"   required
                                   type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="birth_date">Date of Birth</label>
                            <input required type="date" ng-model="selectedItem.birth_date"  name="birth_date" 
                                   class="form-control" id="birth_date" placeholder="DOB">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputAddressStreet">Address Street</label>
                            <input type="text" required name="street" ng-model="selectedItem.address_street"
                                   class="form-control" id="inputAddressStreet" placeholder="Street">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddressNumber">House Number</label>
                            <input type="text" required name="number" ng-model="selectedItem.address_number" 
                                   class="form-control" id="inputAddressNumber" placeholder="House Number">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputTown">Town</label>
                            <input type="text" name="town" ng-model="selectedItem.address_town" 
                                   required class="form-control" id="inputTown">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPostal">Postal Code</label>
                            <input type="text" name="postal_code" ng-model="selectedItem.address_post_code" 
                                   required class="form-control" id="inputPostal">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputRegistration">Registration ID</label>
                            <input type="text" required name="reg_id" ng-model="selectedItem.registration_id" 
                                   class="form-control" id="inputRegistration">
                        </div>
                    </div>
                    <div class="form-row">
                        
                        <div class="form-group col-md-4">
                            <label for="inputGender">Gender</label>
                            <select id="inputGender" name="gender" class="form-control" ng-model="selectedItem.gender"> 
                                <option value="" selected>Choose...</option>
                                <option value="F">Female</option>
                                <option value="M">Male</option>
                            </select>
                        </div>
                        <div class="form-group col-md-8">
                            <span ></span>
                            <label for="inputPicture">Picture
                                <a target="_blank" ng-if="selectedItem.picture" 
                                   href="uploads/{{selectedItem.picture}}">View current picture</a></label>
                            <input type="file" name="picture" accept="image/*" class="form-control" id="inputPicture">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" ng-disabled="processing" 
                            class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </form>
        </div>
    </div>
</div>