<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalTitle">
                    {{"Delete Passenger: " + selectedItem.first_name + " " + selectedItem.last_name}}
                    <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" name="deleteForm" id="deleteForm">
                <div class="modal-body">
                    <p class="text-warning font-weight-bold text-center">{{responseMsg}}</p>
                    <input ng-value="selectedItem.id" type="hidden" name="selected_id">
                    <h3 class="text-center">Sure to Delete?</h3>
                    <p class="text-center">
                        <input required  value="1"
                               type="checkbox" name="delete" class="form-control">
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" ng-disabled="processing" 
                            class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </form>
        </div>
    </div>
</div>