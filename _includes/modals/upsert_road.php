<!-- Add Person Modal -->
<div class="modal fade" id="upsertModal" tabindex="-1" role="dialog" aria-labelledby="upsertModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upsertModalTitle">

                    {{update?"Update "+selectedItem.name:"Add New Road"}}
                    <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" name="upsertForm" id="upsertForm">
                <div class="modal-body">
                    <p class="text-warning font-weight-bold text-center">{{responseMsg}}</p>
                    <input ng-value="selectedItem.id" type="hidden" name="selected_id">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Name</label>
                            <input ng-model="selectedItem.name" type="text"  required
                                   name="name" class="form-control" id="name" placeholder="Name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="class1">Class</label>
                            <select id="class1" name="class" ng-model="selectedItem.road_class" 
                                    class="form-control"> 
                                <option ng-value="" selected>All</option>
                                <option ng-value="'Autobahn/Motorways'">Autobahn/Motorways</option>
                                <option ng-value="'Bundesstraße/Federal Road'">Bundesstraße/Federal Road</option>
                                <option ng-value="'Landesstraße/Regional Road'">Landesstraße/Regional Road</option>
                                <option ng-value="'Kreisstraße/District Road'">Kreisstraße/District Road</option>
                                <option ng-value="'Rural/Secondary'">Rural/Secondary Road</option>
                                <option ng-value="'Other'">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="start_town">Start Town</label>
                            <input ng-model="selectedItem.start_town"   required
                                   type="text" name="start_town" class="form-control" id="start_town" placeholder="Start Town">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="end_town">End Town</label>
                            <input ng-model="selectedItem.end_town"   required
                                   type="text" name="end_town" class="form-control" id="end_town" placeholder="End Town">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="distance">Distance(in KM)</label>
                            <input ng-model="selectedItem.distance"
                                   type="number" name="distance" class="form-control" id="distance" placeholder="Distance">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="speed_limit">Average Speed Limit (in km/hr)</label>
                            <input ng-model="selectedItem.speed_limit"
                                   type="number" name="speed_limit" class="form-control" id="distance" placeholder="Average Speed Limit">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="distance">Remark</label>
                            <textarea ng-model="selectedItem.remark" rows="3"
                                      type="text" name="remark" class="form-control" id="distance" placeholder="remark"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" ng-disabled="processing" 
                            class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </form>
        </div>
    </div>
</div>