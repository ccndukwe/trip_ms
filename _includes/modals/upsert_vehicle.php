<div class="modal fade" id="upsertModal" tabindex="-1" role="dialog" aria-labelledby="upsertModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upsertModalTitle">

                    {{update?"Update "+selectedItem.make+" - "+selectedItem.model:"Add New Vehicle"}}
                    <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" name="upsertForm" id="upsertForm">
                <div class="modal-body">
                    <p class="text-warning font-weight-bold text-center">{{responseMsg}}</p>
                    <input ng-value="selectedItem.id" type="hidden" name="selected_id">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputOwner">Owner</label>
                            <select id="inputOwner" required name="owner_id" class="form-control" ng-model="selectedItem.owner_id"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="item in people" ng-value="{{item.id}}">
                                    {{item.first_name+" "+item.last_name}}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputcategory">Category</label>
                            <select id="inputcategory" required name="category" class="form-control" ng-model="selectedItem.category"> 
                                <option value="" selected>Choose...</option>
                                <option value="SUV" selected>SUV</option>
                                <option value="Sedan" selected>Sedan</option>
                                <option value="Coupe" selected>Coupe</option>
                                <option value="Van/Minivan" selected>Van/Minivan</option>
                                <option value="Van/Minivan" selected>Van/Minivan</option>
                                <option value="Convertible" selected>Convertible</option>
                                <option value="Pickup" selected>Pickup</option>
                                <option value="Hatchback" selected>Hatchback</option>
                                <option value="Coupe, Convertible" selected>Coupe, Convertible</option>
                                <option value="Hatchback, Sedan, Coupe" selected>Hatchback, Sedan, Coupe</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputmake">Make</label>
                            <input ng-model="selectedItem.make"   required
                                   type="text" name="make" class="form-control" id="inputmake" placeholder="Make">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="model">Model</label>
                            <input required type="text" required ng-model="selectedItem.model"  name="model" 
                                   class="form-control" id="model" placeholder="Model">
                        </div>
                         <div class="form-group col-md-4">
                            <label for="year">Yeah</label>
                             <select id="year" required name="year" class="form-control" ng-model="selectedItem.year"> 
                                <option value="" selected>Choose...</option>
                                <option ng-repeat="n in [].constructor(32) track by $index" ng-value="$index+1990">{{$index+1990}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputregistration_numbert">Registration Number</label>
                            <input type="text" required name="reg_number" ng-model="selectedItem.registration_number"
                                   class="form-control" id="inputregistration_number" placeholder="Registration Number">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="registration_date">Registered Date</label>
                            <input type="date" required name="reg_date" ng-model="selectedItem.registration_date" 
                                   class="form-control" id="registration_date" placeholder="Registration Date">
                        </div>
                        <div class="form-group col-md-4">
                            <span ></span>
                            <label for="inputPicture">Picture
                                <a target="_blank" ng-if="selectedItem.picture" 
                                   href="uploads/{{selectedItem.picture}}">View current picture</a></label>
                            <input type="file" name="picture" accept="image/*" class="form-control" id="inputPicture">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" ng-disabled="processing" 
                            class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </form>
        </div>
    </div>
</div>