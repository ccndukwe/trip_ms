<!-- Side bar, with name of the page to ensure the link is made active -->
<!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-car"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Trips Management</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item  <?php if($currentLink=='dashboard'){echo 'active';} ?>">
                <a class="nav-link py-1" href="index.php">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>
               <!-- Divider -->
            <hr class="sidebar-divider">
            
             <li class="nav-item <?php if($currentLink=='people'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="people.php">
                    <i class="fas fa-fw fa-users"></i>
                    <span>People</span></a>
            </li>
            <li class="nav-item <?php if($currentLink=='roads'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="roads.php">
                    <i class="fas fa-fw fa-road"></i>
                    <span>Roads</span></a>
            </li>
            <li class="nav-item <?php if($currentLink=='incident_types'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="incident_types.php">
                    <i class="fas fa-fw fa-list-alt"></i>
                    <span>Incident Types</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
             
            <li class="nav-item <?php if($currentLink=='vehicles'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="vehicles.php">
                    <i class="fas fa-fw fa-train"></i>
                    <span>Vehicles</span></a>
            </li>
             <li class="nav-item <?php if($currentLink=='drivers'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="drivers.php">
                    <i class="fas fa-fw fa-user-astronaut"></i>
                    <span>Drivers</span></a>
            </li>
             <hr class="sidebar-divider">
            <li class="nav-item <?php if($currentLink=='trips'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="trips.php">
                    <i class="fas fa-fw fa-plane-departure"></i>
                    <span>Trips</span></a>
            </li>
            
            <li class="nav-item <?php if($currentLink=='passengers'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="passengers.php">
                    <i class="fas fa-fw fa-user-friends"></i>
                    <span>Passengers</span></a>
            </li>
            
            <li class="nav-item <?php if($currentLink=='incidents'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="incidents.php">
                    <i class="fas fa-fw fa-hospital"></i>
                    <span>Incidents</span></a>
            </li>

            <hr class="sidebar-divider">
            
            <li class="nav-item <?php if($currentLink=='vehicle_ratings'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="vehicle_ratings.php">
                    <i class="fas fa-fw fa-star-and-crescent"></i>
                    <span>Vehicle Ratings</span></a>
            </li>
            
            <li class="nav-item <?php if($currentLink=='driver_ratings'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="driver_ratings.php">
                    <i class="fas fa-fw fa-star-of-life"></i>
                    <span>Driver Ratings</span></a>
            </li>
            
            <li class="nav-item <?php if($currentLink=='road_ratings'){echo 'active';} ?>">
                <a class="nav-link py-1  my-0" href="road_ratings.php">
                    <i class="fas fa-fw fa-star"></i>
                    <span>Road Rating</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->
    
