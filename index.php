<!DOCTYPE html>
<html lang="en">

    <?php
    
    //    This file gives overview of contents in the application database
    
    $currentLink = "dashboard";
    $pageTitle = "Dashboard";
    ?>
    <?php require './_includes/header.php'; ?>

    <?php
    $stmt = $conn->prepare("SELECT COUNT(*) count FROM people");

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }
    $peopleCount = $stmt->get_result()->fetch_assoc()['count'];

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM vehicles");

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }
    $vehiclesCount = $stmt->get_result()->fetch_assoc()['count'];

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM roads");

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }
    $roadsCount = $stmt->get_result()->fetch_assoc()['count'];

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM drivers");

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }
    $driversCount = $stmt->get_result()->fetch_assoc()['count'];

//    

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM trips");

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }
    $tripsCount = $stmt->get_result()->fetch_assoc()['count'];

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM passengers");

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }
    $passengersCount = $stmt->get_result()->fetch_assoc()['count'];

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM incidents");

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }
    $incidentsCount = $stmt->get_result()->fetch_assoc()['count'];

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM incident_types");

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }
    $incidentTypesCount = $stmt->get_result()->fetch_assoc()['count'];

    $stmt = $conn->prepare("SELECT i.*, r.name as roadName,it.name as incidentName, v.registration_number, "
            . "t.start_location, t.start_time, pp.first_name, pp.last_name FROM incidents i "
            . "inner join roads r on i.road_id = r.id inner join trips t on i.trip_id = t.id "
            . "inner join incident_types it on i.incident_type_id = it.id "
            . "inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id "
            . "inner join vehicles v on t.vehicle_id = v.id ORDER BY i.incident_time DESC LIMIT 10");
    
    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $incidents = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    ?>
    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <?php require './_includes/siderbar.php'; ?>

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <?php require './_includes/topbar.php'; ?>
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Content Row -->
                        <div class="row">


                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Registered People</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $peopleCount ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-users fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    Registered Vehicles</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $vehiclesCount ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-bus fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                    Registered Roads</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $roadsCount ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-road fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-danger shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                                    Registered Drivers</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $driversCount ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-user-astronaut fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /.container-fluid -->
                        <div class="row">


                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Registered Trips</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $tripsCount ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-plane-departure fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    Registered Passengers</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $passengersCount ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-user-friends fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                    Registered Incidents</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $incidentsCount ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-hospital fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-danger shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                                    Registered Incident Types</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $incidentTypesCount ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-list-alt fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                          <div class="card border-left-dark shadow p-2">
                        <h4>Recent Incidents</h4>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm small">
                                <thead class="bg-dark-blue">
                                    <tr>
                                        <th>Incident</th>
                                        <th>Incident Time</th>
                                        <th>Road</th>
                                        <th>Trip</th>
                                        <th>Driver</th>
                                        <th class="text-center">Recorded Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($incidents as $incident) { ?>
                                        <tr>
                                            <td><?php echo $incident['incidentName'] ?></td>
                                            <td><?php echo $incident['incident_time'] ?></td>
                                            <td><?php echo $incident['roadName'] ?></td>
                                            <td><?php echo $incident['registration_number']." : ".$incident['start_time']." : ".$incident['start_location'] ?></td>
                                            <td><?php echo $incident['first_name'] . ' ' . $incident['last_name'] ?></td>
                                            <td class="text-center"><?php echo $incident['created_at'] ?></td>

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                    <!-- End of Main Content -->

                    <!-- Footer -->
                       <?php require './_includes/sticky_footer.php'; ?>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

            <?php require './_includes/footer.php'; ?>

    </body>

</html>