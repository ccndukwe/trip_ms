<!DOCTYPE html>
<html lang="en">

    <?php
    
    //    This file is used to display the list of vehicles.
    
    $currentLink = "vehicles";
    $pageTitle = "Vehicles";
    ?>
    <?php require './_includes/header.php'; ?>

    <body ng-app="tripApp" id="page-top" ng-controller="appCtrl" >

        <!-- Page Wrapper -->
        <div id="wrapper">

            <?php require './_includes/siderbar.php'; ?>

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <?php require './_includes/topbar.php'; ?>

                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Content Row -->
                        <div class="row">
                            <div class="col-12 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <span>Search</span>
                                      
                                        <form method="post" class="form-inline"  name="searchForm" id="searchForm">
                                            <label for="inputOwner1">Owner</label>
                                            <select id="inputOwner1" name="owner_id" class="form-control m-1"> 
                                                <option value="" selected>Choose...</option>
                                                <option ng-repeat="item in people" value="{{item.id}}">
                                                    {{item.first_name + " " + item.last_name}}
                                                </option>
                                            </select>
                                            <label for="inputcategory1" class="m-1">Category</label>
                                            <select id="inputcategory1" name="category" class="form-control m-1"> 
                                                <option value="" selected>Choose...</option>
                                                <option value="SUV" >SUV</option>
                                                <option value="Sedan" >Sedan</option>
                                                <option value="Coupe" >Coupe</option>
                                                <option value="Van/Minivan" >Van/Minivan</option>
                                                <option value="Van/Minivan" >Van/Minivan</option>
                                                <option value="Convertible" >Convertible</option>
                                                <option value="Pickup" >Pickup</option>
                                                <option value="Hatchback" >Hatchback</option>
                                                <option value="Coupe, Convertible" >Coupe, Convertible</option>
                                                <option value="Hatchback, Sedan, Coupe" >Hatchback, Sedan, Coupe</option>
                                            </select>
                                            <input type="text" name="make" class="form-control m-1" id="inputmake" placeholder="Make">
                                            <input type="text" name="model" class="form-control m-1" id="model" placeholder="Model">
                                            <label for="year1" class="m-1">Yeah</label>
                                            <select id="year1"  name="year" class="form-control m-1"> 
                                                <option value="" selected>Choose...</option>
                                                <option ng-repeat="n in [].constructor(32) track by $index" value="{{$index + 1990}}">{{$index + 1990}}</option>
                                            </select>
                                            <input type="text" name="reg_number" class="form-control m-1" placeholder="Registration Number">
                                            <label for="inputSeeachDobfrom">Reg. date from</label>
                                            <input type="date"  id="inputSeeachDobfrom"  name="date1" class="form-control m-1" placeholder="Registered Date">
                                            <label for="inputSeeachDobto">Reg. date to</label>
                                            <input type="date"  id="inputSeeachDobto"  name="date2" class="form-control m-1" placeholder="Registered Date">

                                            <button type="submit"
                                                    class="btn btn-primary m-2">  <i ng-show="processing" class="fa fa-spinner fa-spin"></i> Search</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Content Row -->

                        <div class="row">

                            <div class="col-12">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div
                                        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Vehicles 
                                            <i class="small text-black font-weight-lighter" ng-show="result.total">
                                                [Showing {{result.from}} - {{result.to}} of {{result.total}}
                                                result{{result.total>1?'s':''}}]</i>
                                            <i ng-show="!result.total" class="small text-black font-weight-lighter">[No result found]</i>
                                            <i>&nbsp;&nbsp;</i>
                                        </h6>
                                        <div class="dropdown no-arrow">
                                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                                 aria-labelledby="dropdownMenuLink">
                                                <div class="dropdown-header">Actions</div>
                                                <a class="dropdown-item" href="#" ng-click="showUpsertModal(null)" >Add Vehicle</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body pt-0">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-sm small">
                                                <thead class="bg-dark-blue">
                                                    <tr>
                                                        <th></th>
                                                        <th>Owner</th>
                                                        <th>Category</th>
                                                        <th>Make</th>
                                                        <th>Model</th>
                                                        <th>Year</th>
                                                        <th>Registration Number</th>
                                                        <th class="text-center">Avg. Rating</th>
                                                        <th>Registration Date</th>
                                                        <th class="text-center">Recorded Date</th>
                                                        <th class="text-right pr-2">
                                                            Actions
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="each in result.data| orderBy: propertyName: reverse track by $index">
                                                        <td><img ng-if="each.picture" alt="" width="20" src="uploads/{{each.picture}}" /></td>
                                                        <td>{{each.first_name +" "+ each.last_name}}</td>
                                                        <td>{{each.category}}</td>
                                                        <td>{{each.make}}</td>
                                                        <td>{{each.model}}</td>
                                                        <td>{{each.year }}</td>
                                                        <td>{{each.registration_number }}</td>
                                                        <td class="text-center">{{each.average_rating }}</td>
                                                        <td>{{each.registration_date | date: 'dd MMM yyyy' }}</td>
                                                        <td class="text-center">{{each.created_at| date: 'dd MMM yyyy hh:mm a'}}</td>
                                                        <td class="text-center">
                                                            <span class="dropdown pull-right pt-1">
                                                                <button data-toggle="dropdown"
                                                                        class="p-0 btn btn-light dropdown-toggle btn-sm text-dark-blue comment_icon_action pr-1 pl-1 mr-1 ml-1"></button>
                                                                <div class="dropdown-menu rounded pl-3 pr-3">
                                                                    <li class="small"> <a
                                                                            ng-click="showUpsertModal(each)"
                                                                            href="#">Modify</a></li>
                                                                    <div class="dropdown-divider"></div>
                                                                    <li class="small"> <a
                                                                            ng-click="showRatingModal(each)"
                                                                            href="#">Rate</a></li>
                                                                    <div class="dropdown-divider"></div>
                                                                    <li class="small"> <a
                                                                            ng-click="getDetail(each)"
                                                                            href="#">View Details</a></li>
                                                                    <div class="dropdown-divider"></div>
                                                                    <li class="small"> 
                                                                        <a class="text-red"
                                                                           ng-click="showDeleteModal(each)"
                                                                           href="#">Delete</a></li>
                                                                    <div class="dropdown-divider"></div>
                                                                </div>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row" ng-show="result.totalPages > 1">
                                            <div class="col clearfix">
                                                <button ng-show="result.currentPage > 1" type="button"
                                                        ng-click="getPrevious()"
                                                        class="btn btn-light btn-sm float-left">
                                                    <i class="fa fa-arrow-left"></i>&nbsp;Previous
                                                </button>
                                            </div>
                                            <div class="col clearfix">
                                                <button ng-show="result.currentPage < result.totalPages" type="button"
                                                        ng-click="getNext()"
                                                        class="btn btn-light btn-sm float-right">
                                                    Next&nbsp; <i class="fa fa-arrow-right"></i>
                                                </button>
                                            </div>
                                            </
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /.container-fluid -->

                    </div>
                    <!-- End of Main Content -->

                    <!-- Footer -->
                   <?php require './_includes/sticky_footer.php'; ?>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
        </div>
        <!-- End of Page Wrapper -->
        <script src="js/vehicles.js"></script>
        <?php require './_includes/modals/upsert_vehicle.php'; ?>
        <?php require './_includes/modals/delete_vehicle.php'; ?>
        <?php require './_includes/modals/upsert_vehicle_rating.php'; ?>
        <?php require './_includes/footer.php'; ?>
         <div id="modalDiv"></div>
    </div>
</body>

</html>