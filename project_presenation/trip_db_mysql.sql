
/**
* Trip Management Database Application
* for the course Relational Databases
*
* @version: 0.1
* @date: 2021-03-02
* @author: Christian Ndukwe
* @supervisor: Damian Gawenda
*/


---------------------------------------------------------
-- DDL - Data Definition Language (DDL) Statements
---------------------------------------------------------


-- -----------------------------------------------------
-- Drop Tables if exist
-- -----------------------------------------------------
DROP TABLE IF EXISTS people;
DROP TABLE IF EXISTS vehicles;
DROP TABLE IF EXISTS trips;
DROP TABLE IF EXISTS drivers;
DROP TABLE IF EXISTS passengers;
DROP TABLE IF EXISTS roads;
DROP TABLE IF EXISTS incidents;
DROP TABLE IF EXISTS incident_types;
DROP TABLE IF EXISTS vehicle_rating;
DROP TABLE IF EXISTS driver_rating;
DROP TABLE IF EXISTS road_rating;


-- -----------------------------------------------------
-- Drop triggers if exist
-- -----------------------------------------------------
DROP TRIGGER IF EXISTS 'prevent_wrong_incident_time_on_insert';
DROP TRIGGER IF EXISTS 'prevent_wrong_incident_time_on_update';
DROP TRIGGER IF EXISTS 'prevent_future_date_of_birth_insert';
DROP TRIGGER IF EXISTS 'prevent_future_date_of_birth_update';
DROP TRIGGER IF EXISTS 'prevent_future_incident_insert';
DROP TRIGGER IF EXISTS 'prevent_future_incident_update';


-- -----------------------------------------------------
-- Create table mait_trip_db.people
-- -----------------------------------------------------

CREATE TABLE people ( 
id INT PRIMARY KEY AUTO_INCREMENT, 
first_name VARCHAR(100) NOT NULL, 
last_name VARCHAR(100) NOT NULL, 
address_town VARCHAR(45) NOT NULL, 
address_street VARCHAR(45) NOT NULL, 
address_post_code VARCHAR(10) NOT NULL, 
address_number VARCHAR(5) NOT NULL, 
email VARCHAR(100) NOT NULL, 
birth_date DATE NOT NULL, 
picture VARCHAR(255) NULL, 
registration_id VARCHAR(45) NOT NULL, 
gender VARCHAR(1) NULL CHECK (gender IN ('M','F','m','f', null,'')), 
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
CONSTRAINT uq_people_email UNIQUE (email), 
CONSTRAINT uq_people_registration_id UNIQUE (registration_id) ) 


-- -----------------------------------------------------
-- Create table mait_trip_db.vehicles
-- -----------------------------------------------------
CREATE TABLE vehicles (
    id INT PRIMARY KEY AUTO_INCREMENT,
    category  VARCHAR(100) NULL,
    make  VARCHAR(100) NOT NULL,
    model  VARCHAR(45) NOT NULL,
    registration_number  VARCHAR(45) NOT NULL,
    registration_date DATE ,
	year INT NULL,
	owner_id INT NOT NULL,
	picture VARCHAR(255) NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT fk_vehicle_owner FOREIGN KEY (owner_id) REFERENCES people(id),
	CONSTRAINT uq_vehicle_registration_number UNIQUE (registration_number)
);


-- -----------------------------------------------------
-- Create table mait_trip_db.roads
-- -----------------------------------------------------
CREATE TABLE roads (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    road_class VARCHAR(45) NULL,
    distance DECIMAL(8,2) NULL,
    start_town VARCHAR(100) NOT NULL,
    end_town VARCHAR(100) NOT NULL,
    speed_limit DECIMAL(5,2) NULL,
    remark VARCHAR(300) NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- -----------------------------------------------------
-- Create table mait_trip_db.drivers
-- -----------------------------------------------------
CREATE TABLE drivers (
    id INT PRIMARY KEY AUTO_INCREMENT,
    licence_id  VARCHAR(45) NOT NULL,
    licence_class  VARCHAR(45) NOT NULL,
    person_id INT NOT NULL,
	licence_date DATE NULL,
	licence_expiry_date DATE NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_driver_person FOREIGN KEY (person_id) REFERENCES people(id),
	CONSTRAINT uq_driver_licence_id UNIQUE (licence_id),
	CONSTRAINT uq_driver_person_id UNIQUE (person_id),
	CONSTRAINT ck_driver_licence_expiry_date CHECK ((licence_expiry_date > licence_date OR licence_date = NULL OR licence_expiry_date = NULL ))
);

-- -----------------------------------------------------
-- Create table mait_trip_db.trips
-- -----------------------------------------------------
CREATE TABLE trips (
    id INT PRIMARY KEY AUTO_INCREMENT,
    start_location VARCHAR(45) NOT NULL,
    end_location VARCHAR(45) NULL,
    remark VARCHAR(300) NULL,
    route_map VARCHAR(100) NULL,
    vehicle_id INT NOT NULL,
    driver_id INT NOT NULL,
	start_time TIMESTAMP NOT NULL,
	end_time TIMESTAMP NULL,
	max_speed DECIMAL(5,2) NULL,
	average_speed DECIMAL(5,2) NULL,
	distance_covered DECIMAL(8,2) NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_trip_driver FOREIGN KEY (driver_id) REFERENCES drivers(id),
    CONSTRAINT fk_trip_vehicle FOREIGN KEY (vehicle_id) REFERENCES vehicles(id),
	CONSTRAINT ck_trip_end_time CHECK ((end_time > start_time)),
	CONSTRAINT ck_trip_average_speed CHECK ((average_speed <=  max_speed OR average_speed = NULL OR max_speed = NULL ))
);

-- -----------------------------------------------------
-- Create table mait_trip_db.passengers
-- -----------------------------------------------------
CREATE TABLE passengers (
    id INT PRIMARY KEY AUTO_INCREMENT,
    seat_number  VARCHAR(45) NULL,
    person_id INT NOT NULL,
    trip_id INT NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_passenger_person FOREIGN KEY (person_id) REFERENCES people(id),
    CONSTRAINT fk_passenger_trip FOREIGN KEY (trip_id) REFERENCES trips(id),
	CONSTRAINT uq_passenger_seat_number UNIQUE (trip_id,seat_number)
);

-- -----------------------------------------------------
-- Create table mait_trip_db.incident_types
-- -----------------------------------------------------
CREATE TABLE incident_types (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    description TEXT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT uq_incident_type_name UNIQUE (name)
);


-- -----------------------------------------------------
-- Create table mait_trip_db.incidents
-- -----------------------------------------------------
CREATE TABLE incidents (
    id INT PRIMARY KEY AUTO_INCREMENT,
    incident_time TIMESTAMP NOT NULL,
    remark TEXT NULL,
    road_id INT NOT NULL,
    trip_id INT NOT NULL,
    incident_type_id INT NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_incident_road FOREIGN KEY (road_id) REFERENCES roads(id),
    CONSTRAINT fk_incident_trip FOREIGN KEY (trip_id) REFERENCES trips(id),
    CONSTRAINT fk_incident_incident_type_id FOREIGN KEY (incident_type_id) REFERENCES incident_types(id)
);

-- -----------------------------------------------------
-- Create table mait_trip_db.vehicle_rating
-- -----------------------------------------------------
CREATE TABLE vehicle_ratings (
    id INT PRIMARY KEY AUTO_INCREMENT,
    rating INT NOT NULL  CHECK (rating > 0 AND rating <= 5),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    person_id INT NOT NULL,
    vehicle_id INT NOT NULL,
    rating_comment VARCHAR(300) NULL,
    CONSTRAINT fk_user_vehicle_rating FOREIGN KEY (person_id) REFERENCES people(id),
    CONSTRAINT fk_vehicle_rating FOREIGN KEY (vehicle_id) REFERENCES vehicles(id)
);


-- -----------------------------------------------------
-- Create table mait_trip_db.driver_rating
-- -----------------------------------------------------
CREATE TABLE driver_ratings (
    id INT PRIMARY KEY AUTO_INCREMENT,
    rating INT NOT NULL  CHECK (rating > 0 AND rating <= 5),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    person_id INT NOT NULL,
    driver_id INT NOT NULL,
    rating_comment VARCHAR(300) NULL,
    CONSTRAINT fk_user_driver_rating FOREIGN KEY (person_id) REFERENCES people(id),
    CONSTRAINT fk_driver_rating FOREIGN KEY (driver_id) REFERENCES drivers(id)
);

-- -----------------------------------------------------
-- Create table mait_trip_db.road_rating
-- -----------------------------------------------------
CREATE TABLE road_ratings (
    id INT PRIMARY KEY AUTO_INCREMENT,
    rating INT NOT NULL  CHECK (rating > 0 AND rating <= 5),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    person_id INT NOT NULL,
    road_id INT NOT NULL,
    rating_comment VARCHAR(300) NULL,
    CONSTRAINT fk_user_road_rating FOREIGN KEY (person_id) REFERENCES people(id),
    CONSTRAINT fk_road_rating FOREIGN KEY (road_id) REFERENCES roads(id)
);

-- -----------------------------------------------------
-- Prevent future date of birth during insert on mait_trip_db.people
-- -----------------------------------------------------
DELIMITER $$
CREATE TRIGGER `prevent_future_date_of_birth_insert` 
BEFORE INSERT ON `people` 
FOR EACH ROW   
BEGIN
IF (NEW.birth_date > CURRENT_DATE) 
THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Date of Birth cannot be future';
END IF;
END $$
DELIMITER ;

-- -----------------------------------------------------
-- Prevent future date of birth during update on mait_trip_db.people
-- -----------------------------------------------------
DELIMITER $$
CREATE TRIGGER `prevent_future_date_of_birth_update` 
BEFORE UPDATE ON `people` 
FOR EACH ROW 
BEGIN
IF (NEW.birth_date > CURRENT_DATE) 
THEN 
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Date of Birth cannot be future'; 
END IF;
END $$ 
DELIMITER ;


-- -----------------------------------------------------
-- Prevent incident start time that is less than trip start time or
-- after trip end time during insert on mait_trip_db.incidents
-- -----------------------------------------------------
DELIMITER $$
CREATE TRIGGER `prevent_wrong_incident_time_on_insert` 
BEFORE INSERT ON `incidents` 
FOR EACH ROW 
BEGIN
IF (NEW.incident_time < (SELECT start_time from trips where id = NEW.trip_id) OR NEW.incident_time > (SELECT end_time from trips where id = NEW.trip_id)) 
THEN 
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Incident time cannot be before or after the trip'; 
END IF;
END $$ 
DELIMITER ;

-- -----------------------------------------------------
-- Prevent incident start time that is less than trip start time or
-- after trip end time during update on mait_trip_db.incidents
-- -----------------------------------------------------
DELIMITER $$
CREATE TRIGGER `prevent_wrong_incident_time_on_update` 
BEFORE UPDATE ON `incidents` 
FOR EACH ROW 
BEGIN
IF (NEW.incident_time < (SELECT start_time from trips where id = NEW.trip_id) OR NEW.incident_time > (SELECT end_time from trips where id = NEW.trip_id)) 
THEN 
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Incident time cannot be before or after the trip'; 
END IF;
END $$ 
DELIMITER ;

-- -----------------------------------------------------
-- Prevent future incident_time during insert on mait_trip_db.incidents
-- -----------------------------------------------------
DELIMITER $$
CREATE TRIGGER `prevent_future_incident_insert` 
BEFORE INSERT ON `incidents` 
FOR EACH ROW 
BEGIN
IF (NEW.incident_time > CURRENT_TIMESTAMP) 
THEN 
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Incident time cannot be future'; 
END IF;
END $$ 
DELIMITER ;


-- -----------------------------------------------------
-- Prevent future incident_time during update on mait_trip_db.incidents
-- -----------------------------------------------------
DELIMITER $$
CREATE TRIGGER `prevent_future_incident_update` 
BEFORE  UPDATE ON `incidents` 
FOR EACH ROW 
BEGIN
IF (NEW.incident_time > CURRENT_TIMESTAMP) 
THEN 
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Incident time cannot be future'; 
END IF;
END $$ 
DELIMITER ;


-- -----------------------------------------------------
-- Prevent future date vehicle_registration_date during insert on mait_trip_db.vehicles
-- -----------------------------------------------------
DELIMITER $$
CREATE TRIGGER `prevent_future_vehicle_registration_date_insert` 
BEFORE INSERT ON `vehicles` 
FOR EACH ROW  
BEGIN 
IF (NEW.registration_date > CURRENT_DATE) 
THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Vehicle Registered date cannot be future';
END IF;
END $$ 
DELIMITER ;

-- -----------------------------------------------------
-- Prevent future driver vehicle_registration_date during update on mait_trip_db.vehicles
-- -----------------------------------------------------
DELIMITER $$
CREATE TRIGGER `prevent_future_vehicle_registration_date_update` 
BEFORE UPDATE ON `vehicles` 
FOR EACH ROW 
BEGIN 
IF (NEW.registration_date > CURRENT_DATE) 
THEN 
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Vehicle Registered date cannot be future';
END IF; 
END $$ 
DELIMITER ;



-- -----------------------------------------------------
-- Prevent future driver registration_date  during insert on mait_trip_db.drivers
-- -----------------------------------------------------
DELIMITER $$
CREATE TRIGGER `prevent_future_license_date_insert` 
BEFORE INSERT ON `drivers` 
FOR EACH ROW  
BEGIN  
IF (NEW.licence_date > CURRENT_DATE) 
THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'License date cannot be future';
END IF; 
END $$ 
DELIMITER

-- -----------------------------------------------------
-- Prevent future driver registration_date during update on mait_trip_db.drivers
-- -----------------------------------------------------
DELIMITER $$
CREATE TRIGGER `prevent_future_license_date_update` 
BEFORE UPDATE ON `drivers` 
FOR EACH ROW 
BEGIN 
IF (NEW.licence_date > CURRENT_DATE) 
THEN 
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'License date cannot be future';
END IF; 
END $$ 
DELIMITER



---------------------------------------------------------
-- DML - Data Manipulation Language (DML) Statements
---------------------------------------------------------
 
-- -----------------------------------------------------
-- Insert into mait_trip_db.people
-- -----------------------------------------------------
 
 INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Arbert', 'Murray', 'abert.murray@dbtrip.com', 'Overath', 
					'Ostram','1', '456782', '1990-01-01', NULL, '100000001', 'M');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
            VALUES ('Kaser', 'Stone', 'kaser.stone@dbtrip.com', 'Overath', 
					'Ostram','14', '407782', '1990-03-01', NULL, '100000002', 'M');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Charity', 'Wilson', 'charity.wilson@dbtrip.com', 'Cologne', 
					'Kassle','3', '456681', '1983-03-01', NULL, '200000001', 'F');	
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('John', 'Cross', 'john.cross@dbtrip.com', 'Cologne', 
					'Mater','4', '426681', '1993-03-09', NULL, '200000002', 'M');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Chris', 'Water', 'chris.water@dbtrip.com', 'Cologne', 
					'Roadix','5', '427781', '1991-03-09', NULL, '200000003', 'M');	
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('George', 'Series', 'george.series@dbtrip.com', 'Cologne', 
					'Roadix','7', '421185', '1991-03-09', NULL, '200000004', 'M');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Mantel', 'College', 'mantel.college@dbtrip.com', 'Gummersbach', 
					'Ostram','7', '561183', '1990-02-09', NULL, '300000001', 'F');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Karis', 'Mann', 'karis.mann@dbtrip.com', 'Gummersbach', 
					'thraser','5', '5610083', '1993-10-09', NULL, '300000002', 'M');								
 INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('George', 'Murray', 'george.murray@dbtrip.com', 'Berlin', 
					'Ostram','11', '12345', '1991-08-01', NULL, '400000011', 'F');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
            VALUES ('Miracle', 'Karis', 'miracle.karis@dbtrip.com', 'Overath', 
					'Binstrate','14', '407782', '1990-03-01', NULL, '600000012', 'M');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Love', 'Kalu', 'love.kalu@dbtrip.com', 'Cologne', 
					'Obi','13', '456681', '1989-03-01', NULL, '200000111', 'F');	
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Mike', 'Anderson', 'mike.anderson@dbtrip.com', 'Cologne', 
					'Karris','40', '426681', '1993-03-09', NULL, '100000012', 'M');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Water', 'Innocent', 'water.innocent@dbtrip.com', 'Cologne', 
					'Stratze','15', '420781', '1994-03-09', NULL, '500000003', 'M');	
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Lion', 'Stone', 'lion.stone@dbtrip.com', 'Cologne', 
					'Multiple','17', '421185', '1981-03-09', NULL, '400000004', 'M');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Oliva', 'Kassle', 'oliva.college@dbtrip.com', 'Gummersbach', 
					'Kate','17', '561183', '1990-02-09', NULL, '300000012', 'F');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Karis', 'Mann', 'karis.mann@dbtrip.com', 'Gummersbach', 
					'Gummback','15', '5810083', '1893-10-09', NULL, '700000012', 'M');
 INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Charity', 'Wilton', 'charity.wilton@dbtrip.com', 'Berlin', 
					'Stracity','11', '121345', '1990-08-01', NULL, '400300011', 'F');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
            VALUES ('Chris', 'Karis', 'chris.karis@dbtrip.com', 'Overath', 
					'Binstrate','14', '407782', '1990-03-01', NULL, '6000100012', 'M');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Cosmos', 'Kalu', 'cosmos.kalu@dbtrip.com', 'Cologne', 
					'Obi','13', '456681', '1989-03-01', NULL, '200100111', 'F');	
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Stone', 'Anderson', 'stone.anderson@dbtrip.com', 'Cologne', 
					'Karris','40', '426681', '1993-03-09', NULL, '100010012', 'M');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Micheal', 'Innocent', 'mike.innocent@dbtrip.com', 'Cologne', 
					'Stratze','15', '420781', '1994-03-09', NULL, '500100003', 'M');	
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Darlington', 'Stone', 'darlington.stone@dbtrip.com', 'Cologne', 
					'Multiple','17', '421185', '1985-03-09', NULL, '400001004', 'M');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Kelvin', 'Kassle', 'kelvin.college@dbtrip.com', 'Gummersbach', 
					'Kate','17', '561183', '1990-02-09', NULL, '300000012', 'F');
INSERT INTO people (first_name, last_name, email, address_town, address_street, address_number, 
					address_post_code, birth_date, picture, registration_id, gender) 
			VALUES ('Monday', 'Mann', 'monday.mann@dbtrip.com', 'Gummersbach', 
					'Gummback','15', '5810083', '1893-10-09', NULL, '700010012', 'M');


 -- -----------------------------------------------------
-- Insert into mait_trip_db.vehicles
-- -----------------------------------------------------
INSERT INTO vehicles (category, make, model, registration_number, owner_id, registration_date, year)
			VALUES ('SUV', 'Audi', 'Q3', '00000001', 1,  '2020-01-27', 2020);
INSERT INTO vehicles (category, make, model, registration_number, owner_id, registration_date, year)
			VALUES ('Sedan', 'Chevrolet', 'Malibu', '00000002', 1, '2020-01-27', 2020);
INSERT INTO vehicles (category, make, model, registration_number, owner_id, registration_date, year)
			VALUES ( 'SUV', 'Cadillac', 'Escalade ESV', '00000003', 2, '2020-01-27', 2020);
INSERT INTO vehicles (category, make, model, registration_number, owner_id, registration_date, year)
			VALUES ( 'Coupe, Convertible', 'Chevrolet', 'Corvette', '00000004', 4, '2020-01-27', 2020);
INSERT INTO vehicles (category, make, model, registration_number, owner_id, registration_date, year)
			VALUES ( 'Sedan', 'Acura', 'RLX', '00000005', 3, '2020-01-27', 2020);
INSERT INTO vehicles (category, make, model, registration_number, owner_id, registration_date, year)
			VALUES ( 'Pickup', 'Chevrolet', 'Silverado 2500 HD Crew Cab', '00000006', 1, '2020-01-27', 2020);
INSERT INTO vehicles (category, make, model, registration_number, owner_id, registration_date, year)
			VALUES ( 'Sedan', 'BMW', '3 Series', '00000007', 3,  '2020-01-27', 2020);
INSERT INTO vehicles (category, make, model, registration_number, owner_id, registration_date, year)
			VALUES ( 'Van/Minivan', 'Chrysler', 'Pacifica', '00000008', 1,  '2020-01-27', 2020);
INSERT INTO vehicles (category, make, model, registration_number, owner_id, registration_date, year)
			VALUES ( 'Pickup', 'Chevrolet', 'Colorado Crew Cab', '00000009', 4,  '2020-01-27', '2020');
INSERT INTO vehicles (category, make, model, registration_number, owner_id, registration_date, year)
			VALUES ( 'SUV', 'BMW', 'X3', '00000010', 5,  '2020-01-27', '2020');
	
	
-- -----------------------------------------------------
-- Insert into mait_trip_db.roads
-- -----------------------------------------------------
INSERT INTO roads (name, road_class, start_town,end_town, distance, speed_limit)
			VALUES ('Linksrheinische Autobahn','Autobahn', 'Heiligenhafen', 'Blankenheim', 749, 100);
INSERT INTO roads (name, road_class, start_town,end_town, distance, speed_limit)
			VALUES ('Warschauer Allee', 'Autobahn', 'AK Oberhause', 'AD Werder', 473, 100);
INSERT INTO roads (name, road_class, start_town,end_town, distance, speed_limit)
			VALUES ( 'Rechtsrheinische Autobahn', 'Autobahn','Elten',' Pocking', 769, 120);
INSERT INTO roads (name, road_class, start_town,end_town, distance, speed_limit)
			VALUES ( 'Kölner Ring', 'Autobahn','Aachen','Krombach', 583, 130);
INSERT INTO roads (name, road_class, start_town,end_town, distance, speed_limit)
			VALUES ( 'Bergstraßenautobahn', 'Autobahn','Hattenbacher Dreieck','Weil am Rhein', 440, 120);
INSERT INTO roads (name, road_class, start_town,end_town, distance, speed_limit)
			VALUES ( 'Kraichgau-Autobahn', 'Autobahn','Saarbrücken','Waidhaus', 484, 120);
INSERT INTO roads (name, road_class, start_town,end_town, distance, speed_limit)
			VALUES ( 'Rhönautobahn', 'Autobahn','Handewitt','Füssen', 962, 120);
			
			
			
---------------------------------------------------------
-- Insert into mait_trip_db.drivers
-- ------------------------------------------------------
INSERT INTO drivers (licence_id, licence_class, person_id, licence_date, licence_expiry_date)
			VALUES ('BO4848943981', 'B',1, '2000-03-09','2020-03-09');
INSERT INTO drivers (licence_id, licence_class,  person_id,licence_date, licence_expiry_date)
			VALUES ('BO4848943001', 'D',2 , '2001-03-09','2021-03-09');
INSERT INTO drivers (licence_id, licence_class,  person_id,licence_date, licence_expiry_date)
			VALUES ('DO4848943995', 'D',3 , '2002-03-09','2022-03-09');
INSERT INTO drivers (licence_id, licence_class,  person_id,licence_date, licence_expiry_date)
			VALUES ('DO4848000995', 'B',4 , '2003-03-09','2023-03-09');
INSERT INTO drivers (licence_id, licence_class, person_id, licence_date, licence_expiry_date)
			VALUES ('DO48489439007', 'D',5 , '2004-03-09','2023-03-09');
	

	
---------------------------------------------------------
-- Insert into mait_trip_db.trips
-- ------------------------------------------------------
INSERT INTO trips (start_location, end_location, vehicle_id, driver_id, start_time, end_time, max_speed, average_speed, distance_covered)
			VALUES ('Lübeck', 'Bremen',1,3, '2020-01-09 08:13','2020-01-09 09:02',120, 85, 50);
INSERT INTO trips (start_location, end_location, vehicle_id, driver_id, start_time, end_time, max_speed, average_speed, distance_covered)
			VALUES ('Dortmund', 'Euskirchen',1,3, '2020-02-19 08:13','2020-02-19 10:01',110, 80, 100);
INSERT INTO trips (start_location, end_location, vehicle_id, driver_id, start_time, end_time, max_speed, average_speed, distance_covered)
			VALUES ('Wesel ', 'Hannover ',2,4, '2020-03-09 08:13','2020-03-09 12:15',100, 90, 70);
INSERT INTO trips (start_location, end_location, vehicle_id, driver_id, start_time, end_time, max_speed, average_speed, distance_covered)
			VALUES ('Pocking', 'Würzburg',3,4, '2020-04-20 08:13','2020-04-20 13:11',100, 50, 75);
INSERT INTO trips (start_location, end_location, vehicle_id, driver_id, start_time, end_time, max_speed, average_speed, distance_covered)
			VALUES ('Frankfurt', 'Aachen',3,5, '2020-05-01 08:13','2020-05-01 10:03',80, 65, 40);
INSERT INTO trips (start_location, end_location, vehicle_id, driver_id, start_time, end_time, max_speed, average_speed, distance_covered)
			VALUES ('Gummersbach', 'Köln',4,6, '2020-03-10 08:13','2020-03-10 20:10',120, 95, 45);	


---------------------------------------------------------
-- Insert into mait_trip_db.passengers
-- ------------------------------------------------------
INSERT INTO passengers (seat_number, person_id, trip_id)
			VALUES ('A002', 2,1);
INSERT INTO passengers (seat_number, person_id, trip_id)
			VALUES ('B010', 1,4);
INSERT INTO passengers (seat_number, person_id, trip_id)
			VALUES ('A020', 2,4);
INSERT INTO passengers (seat_number, person_id, trip_id)
			VALUES ('A022', 1,4);
INSERT INTO passengers (seat_number, person_id, trip_id)
			VALUES ('A031', 2,1);
INSERT INTO passengers (seat_number, person_id, trip_id)
			VALUES ('A005', 3,1);
			
			

---------------------------------------------------------
-- Insert into mait_trip_db.incident_types
-- ------------------------------------------------------
INSERT INTO incident_types (name, description)
			VALUES ('Vehicle Rollover', 'These particular types of crashes are complex and violent in nature. More than any other type of crash, rollovers reflect the interaction of the driver, road, vehicle, and environmental factors. Although a vehicle’s type has a significant role in the accident, so does driver behavior and road and environmental conditions. Other factors include speed, alcohol consumption, and location. According to the NHTSA, data showed that nearly 85% of all rollover-related fatalities are the result of single-vehicle crashes.');
INSERT INTO incident_types (name, description)
			VALUES ('Single Car Accident', 'This is a type of road traffic accident in which only one vehicle is involved. A majority of these types of crashes are run-off-road collisions, collisions with fallen debris, rollovers, and collisions with animals.');
INSERT INTO incident_types (name, description)
			VALUES ('Rear-End Collision', 'A traffic accident where a vehicle crashes into the vehicle in front of it. These are usually due to driver inattention or distraction, tailgating, panic stops, and reduced traction due to irregular road conditions caused by weather.');
INSERT INTO incident_types (name, description)
			VALUES ('Side-Impact Collision', 'These accidents, also known as broadside or T-bone collisions, are where the side of one or more vehicles is impacted. These crashes commonly occur at intersections, parking lots, and when two vehicles pass on a roadway accounting for about a quarter of passenger vehicle occupant deaths, according to the IIHS. The results from a side-impact collision can be severe but can vary depending on where the vehicle is struck.');
INSERT INTO incident_types (name, description)
			VALUES ('Head-On Collision', 'This type of accident is when the front ends of two vehicles hit each other in opposite directions. Head-on collisions are often fatal road traffic accidents. Being aware of traffic signs, street conditions, and staying in your lane play a critical role in avoiding these types of accidents.');
INSERT INTO incident_types (name, description)
			VALUES ('Ignoring Traffic rules', 'Ignoring traffic rules such as jumping red signals, driving over the speed limit, driving in the wrong lane. Ignoring traffic rules not only jeopardizes the life of the driver but also others around them. If you jump a red light because you are late, doesn’t necessarily mean you reach your destination in time. In fact ignoring traffic rules will only result in further issues consequently which will make you even more lately for your appointment.');
INSERT INTO incident_types (name, description)
			VALUES ('Distracted Driving', 'Although driving while being can seem like a small thing but even losing your focus for a tiny moment can cause a lot of damage. Nowadays technology is the main distraction such as changing the music channel etc. Drivers are constantly checking their mobile phones, GPS other devices for directions. Even passengers in the car or eating, drinking coffee or any other drink can be a reason for the driver to get distracted as well.');
			

---------------------------------------------------------
-- Insert into mait_trip_db.incidents
-- ------------------------------------------------------
INSERT INTO incidents (road_id, trip_id, incident_type_id,  incident_time, remark)
			VALUES ( 1, 1, 6, '2020-01-09 09:13', 'Over-speeding');
INSERT INTO incidents (road_id, trip_id, incident_type_id,  incident_time, remark)
			VALUES ( 1, 2, 1, '2020-02-19 09:13', NULL);
INSERT INTO incidents (road_id, trip_id, incident_type_id,  incident_time, remark)
			VALUES ( 1, 3, 1, '2020-03-15 10:13', NULL);
INSERT INTO incidents (road_id, trip_id, incident_type_id,  incident_time, remark)
			VALUES ( 1, 1, 2, '2020-03-09 09:13', NULL);
INSERT INTO incidents (road_id, trip_id, incident_type_id,  incident_time, remark)
			VALUES ( 1, 2, 6, '2020-05-01 09:13', 'Running trafic light');
INSERT INTO incidents (road_id, trip_id, incident_type_id,  incident_time, remark)
			VALUES ( 1, 4, 6, '2020-04-20 09:13', 'Wrong packing');
INSERT INTO incidents (road_id, trip_id, incident_type_id,  incident_time, remark)
			VALUES ( 1, 4, 6, '2020-05-01 09:13', NULL);
			
			
---------------------------------------------------------
-- Insert into mait_trip_db.vehicle_rating
-- ------------------------------------------------------			
INSERT INTO vehicle_ratings ( rating, person_id, vehicle_id, rating_comment)
			VALUES ( 4, 1, 1, 'Good design');
INSERT INTO vehicle_ratings ( rating, person_id, vehicle_id, rating_comment)
			VALUES ( 5, 2, 2, 'One of the best vehicle I ever used');
INSERT INTO vehicle_ratings ( rating, person_id, vehicle_id, rating_comment)
			VALUES ( 3, 1, 3, 'Not good for long trip');
INSERT INTO vehicle_ratings ( rating, person_id, vehicle_id, rating_comment)
			VALUES ( 2, 1, 1, 'Too warm');



---------------------------------------------------------
-- Insert into mait_trip_db.driver_ratings
-- ------------------------------------------------------
INSERT INTO driver_ratings ( rating, person_id, driver_id, rating_comment)
			VALUES (2, 2, 1, 'Not listening to passengers');
INSERT INTO driver_ratings ( rating, person_id, driver_id)
			VALUES (4, 1, 2);
INSERT INTO driver_ratings ( rating, person_id, driver_id, rating_comment)
			VALUES (5, 1, 2, 'Great driver');
INSERT INTO driver_ratings ( rating, person_id, driver_id)
			VALUES (4, 4, 1);
			

---------------------------------------------------------
-- Insert into mait_trip_db.road_ratings
-- ------------------------------------------------------
INSERT INTO road_ratings ( rating, person_id, road_id, rating_comment)
			VALUES (2, 2, 1, 'Bad conners');
INSERT INTO road_ratings ( rating, person_id, road_id)
			VALUES (4, 1, 2);
INSERT INTO road_ratings ( rating, person_id, road_id, rating_comment)
			VALUES (5, 1, 2, 'Awesome terrain');
INSERT INTO road_ratings ( rating, person_id, road_id)
			VALUES (4, 4, 1);	


---------------------------------------------------------
-- UPDATE queries. ? represent placeholders for the actual data
-- ------------------------------------------------------
UPDATE drivers SET licence_id = ?, licence_class = ?, person_id = ?, 
                 licence_date = ?, licence_expiry_date = ? WHERE id = ?;
				
UPDATE driver_ratings SET rating = ?, 
                 person_id = ?, driver_id = ?, rating_comment = ?  WHERE id = ?

UPDATE incidents SET incident_time = ?, remark = ?, 
                 road_id = ?, trip_id = ?, incident_type_id = ?  WHERE id = ?

UPDATE incident_types SET name = ?, description = ? WHERE id = ?

UPDATE passengers SET seat_number = ?, 
                person_id = ?, trip_id = ?  WHERE id = ?

UPDATE people SET first_name = ?, last_name = ?, address_street = ?, address_number = ?, 
                 address_town = ?, address_post_code = ?, birth_date = ?, email = ?, 
                 gender = ?, registration_id= ? WHERE id = ?

UPDATE roads SET name = ?, road_class = ?, distance = ?, start_town = ?, 
                 end_town = ?, speed_limit = ?, remark = ? WHERE id = ?

UPDATE road_ratings SET rating = ?, 
                 person_id = ?, road_id = ?, rating_comment = ?  WHERE id = ?;

UPDATE trips SET start_location = ?, end_location = ?, remark = ?, 
                 vehicle_id = ?, driver_id = ?, start_time = ?,end_time = ?, 
                max_speed = ?,average_speed=?,distance_covered=?  WHERE id = ?;

UPDATE vehicles SET category = ?, make = ?, model = ?, registration_number = ?, 
                 registration_date = ?, owner_id = ? , year = ? WHERE id = ?


UPDATE vehicle_ratings SET rating = ?, 
                 person_id = ?, vehicle_id = ?, rating_comment = ?  WHERE id = ?
				
				
---------------------------------------------------------
-- DELETE queries. ? represent placeholders for the actual data
-- ------------------------------------------------------
DELETE FROM drivers WHERE id = ?;
DELETE FROM driver_ratings WHERE id = ?;
DELETE FROM trips WHERE id = ?;
DELETE FROM incident_types WHERE id = ?;
DELETE FROM passengers WHERE id = ?;
DELETE FROM people WHERE id = ?;
DELETE FROM roads WHERE id = ?;
DELETE FROM road_ratings WHERE id = ?;
DELETE FROM trips WHERE id = ?;
DELETE FROM vehicles WHERE id = ?;
DELETE FROM driver_ratings WHERE id = ?;



---------------------------------------------------------
-- DQL - Data QUER Language (DQL) Statements
---------------------------------------------------------

---------------------------------------------------------
--SQL to get the driver personal information and average ratings. Multiple tables are combined to get result
---------------------------------------------------------
SELECT d.*,(SELECT AVG(rating) from driver_ratings 
            where driver_id = d.id) average_rating, p.first_name, p.last_name, p.picture, p.birth_date 
             FROM drivers d inner join people p on d.person_id = p.id WHERE d.id = ?;


---------------------------------------------------------
--SQL to get the incident information related to the driver. Multiple tables are combined to get result
---------------------------------------------------------
SELECT i.*, r.name as roadName,it.name as incidentName, v.registration_number, 
             t.start_location, t.start_time, pp.first_name, pp.last_name FROM incidents i 
             inner join roads r on i.road_id = r.id inner join trips t on i.trip_id = t.id 
             inner join incident_types it on i.incident_type_id = it.id 
             inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id 
             inner join vehicles v on t.vehicle_id = v.id WHERE t.driver_id = ? ORDER BY i.created_at DESC LIMIT 50;


---------------------------------------------------------
--SQL to get the driver ratings. Multiple tables are combined to get result
---------------------------------------------------------
SELECT r.*, p.first_name, p.last_name FROM driver_ratings r 
            inner join people p on r.person_id = p.id 
             WHERE r.driver_id = ? ORDER BY r.created_at DESC LIMIT 50;

---------------------------------------------------------
--SQL to get the driver trips. Multiple tables are combined to get result
---------------------------------------------------------
SELECT t.*, v.registration_number FROM trips t 
              inner join vehicles v on t.vehicle_id = v.id  
              WHERE t.driver_id = ? ORDER BY t.created_at DESC LIMIT 50;

---------------------------------------------------------
--SQL to list driver ratings. Multiple tables are combined to get result
---------------------------------------------------------
SELECT dr.*, p.first_name,p.last_name, pd.first_name as driverFirstName,  
         pd.last_name as driverLastName FROM driver_ratings dr 
         inner join drivers d on dr.driver_id = d.id inner join people p on 
         dr.person_id = p.id inner join people pd on d.person_id = pd.id  LIMIT ?, ?;

---------------------------------------------------------
--SQL to list drivers. Multiple tables are combined to get result
---------------------------------------------------------
SELECT d.*, p.first_name, p.last_name, 
        (SELECT AVG(rating) from driver_ratings where driver_id = d.id) average_rating 
        FROM drivers d inner join people p on d.person_id = p.id;
		
		
---------------------------------------------------------
--SQL to list incident types
---------------------------------------------------------
SELECT * FROM incident_types where name LIKE ?;

---------------------------------------------------------
--SQL to list incidents. Multiple tables are combined to get result
---------------------------------------------------------
SELECT i.*, r.name as roadName,it.name as incidentName, v.registration_number, 
        t.start_location, t.start_time, pp.first_name, pp.last_name FROM incidents i 
        inner join roads r on i.road_id = r.id inner join trips t on i.trip_id = t.id 
        inner join incident_types it on i.incident_type_id = it.id 
        inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id 
        inner join vehicles v on t.vehicle_id = v.id;
		
		
---------------------------------------------------------
--SQL to list passengers. Multiple tables are combined to get result
---------------------------------------------------------
SELECT pg.*, tp.start_location,tp.end_location, tp.start_time,tp.end_time, 
        pp.first_name, pp.last_name  FROM passengers pg 
        inner join trips tp on pg.trip_id = tp.id 
        inner join people pp on pg.person_id = pp.id where trip_id = ?;

---------------------------------------------------------
--SQL to list all people in a particuler town.
---------------------------------------------------------
SELECT * FROM people WHERE town = ?;

---------------------------------------------------------
--SQL to get the trips taken by a person. Multiple tables are combined to get result
---------------------------------------------------------
SELECT p.*, t.start_time, t.start_location, t.end_time, 
            t.end_location, pp.first_name, pp.last_name, v.registration_number 
             FROM passengers p inner join trips t on p.trip_id = t.id 
            inner join drivers d on t.driver_id = d.id 
            inner join people pp on d.person_id = pp.id 
            inner join vehicles v on t.vehicle_id = v.id 
             WHERE p.person_id = ? ORDER BY p.created_at DESC LIMIT 50;

---------------------------------------------------------
--SQL to get vehicles owned by taken by a person. Multiple tables are combined to get result
---------------------------------------------------------
SELECT v.*, (SELECT AVG(rating) from vehicle_ratings where vehicle_id = v.id) average_rating 
FROM vehicles v WHERE v.owner_id = ?;

---------------------------------------------------------
--SQL to get personal detail of one person by id
---------------------------------------------------------
SELECT * FROM people WHERE id = ?;

---------------------------------------------------------
--SQL to get the trip information and average ratings. Multiple tables are combined to get result
---------------------------------------------------------
SELECT r.*, (SELECT AVG(rating) from road_ratings where road_id = r.id) average_rating 
FROM roads r WHERE id = ?;

---------------------------------------------------------
--SQL to get incidents associated with a particular trip. Multiple tables are combined to get result
---------------------------------------------------------
SELECT i.*, r.name as roadName,it.name as incidentName, v.registration_number, 
            t.start_location, t.start_time, pp.first_name, pp.last_name FROM incidents i 
            inner join roads r on i.road_id = r.id inner join trips t on i.trip_id = t.id 
            inner join incident_types it on i.incident_type_id = it.id 
            inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id 
            inner join vehicles v on t.vehicle_id = v.id WHERE i.road_id = ? ORDER BY i.created_at DESC LIMIT 50;
			

---------------------------------------------------------
--SQL to get the road ratings. Multiple tables are combined to get result
---------------------------------------------------------
SELECT r.*, p.first_name, p.last_name FROM road_ratings r 
            inner join people p on r.person_id = p.id 
             WHERE r.road_id = ? ORDER BY r.created_at DESC LIMIT 50;

---------------------------------------------------------
--SQL to get list road ratings. Multiple tables are combined to get result
---------------------------------------------------------
SELECT rr.*, p.first_name,p.last_name,r.name as roadName FROM road_ratings rr 
        inner join roads r on rr.road_id = r.id inner join people p on rr.person_id = p.id;


---------------------------------------------------------
--SQL to list roads. Multiple tables are combined to get result
---------------------------------------------------------
SELECT r.*, (SELECT AVG(rating) from road_ratings where road_id = r.id) average_rating FROM roads r;

---------------------------------------------------------
--SQL to list roads. Multiple tables are combined to get result
---------------------------------------------------------
SELECT r.*, (SELECT AVG(rating) from road_ratings where road_id = r.id) average_rating FROM roads r; 	

---------------------------------------------------------
--SQL to trip detail. Multiple tables are combined to get result
---------------------------------------------------------
SELECT t.*, d.licence_id, v.make, v.model, v.year, v.registration_number, 
             p.first_name as driverFirstName, p.last_name as driverLastName, 
             o.first_name as ownerFirstName, o.last_name as ownerLastName FROM trips t 
            inner join drivers d on t.driver_id = d.id inner join  people p on d.person_id = p.id 
            inner join vehicles v on t.vehicle_id = v.id 
            inner join people o on v.owner_id = o.id WHERE t.id = ?;
			
			
---------------------------------------------------------
--SQL to road incidents associated with a trip. Multiple tables are combined to get result
---------------------------------------------------------
SELECT i.*, r.name as roadName,it.name as incidentName, v.registration_number, 
            t.start_location, t.start_time, pp.first_name, pp.last_name FROM incidents i 
            inner join roads r on i.road_id = r.id inner join trips t on i.trip_id = t.id 
            inner join incident_types it on i.incident_type_id = it.id 
            inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id 
            inner join vehicles v on t.vehicle_id = v.id WHERE t.id = ? ORDER BY i.created_at DESC LIMIT 50;
			
			
---------------------------------------------------------
--SQL to trip passengers. Multiple tables are combined to get result
---------------------------------------------------------

SELECT p.*, pp.first_name, pp.last_name FROM passengers p 
            inner join people pp on p.person_id = pp.id 
             WHERE p.trip_id = ? ORDER BY pp.first_name;
			
---------------------------------------------------------
--SQL to list trips. Multiple tables are combined to get result
---------------------------------------------------------
SELECT t.*, d.licence_id, v.make, v.model, v.year, v.registration_number, p.first_name, p.last_name  FROM trips t 
         inner join drivers d on t.driver_id = d.id inner join  people p on d.person_id = p.id 
        inner join vehicles v on t.vehicle_id = v.id;
		

---------------------------------------------------------
--SQL to get vehicle detail. Multiple tables are combined to get result
---------------------------------------------------------
SELECT v.*, p.first_name, p.last_name, 
            (SELECT AVG(rating) from vehicle_ratings where vehicle_id = v.id) average_rating 
            FROM vehicles v inner join people p on v.owner_id = p.id WHERE v.id = ?;
			
---------------------------------------------------------
--SQL to list incidents associated with a vehicle. Multiple tables are combined to get result
---------------------------------------------------------
SELECT i.*, r.name as roadName,it.name as incidentName, v.registration_number, 
            t.start_location, t.start_time, pp.first_name, pp.last_name FROM incidents i 
            inner join roads r on i.road_id = r.id inner join trips t on i.trip_id = t.id 
            inner join incident_types it on i.incident_type_id = it.id 
            inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id 
            inner join vehicles v on t.vehicle_id = v.id WHERE t.vehicle_id = ? ORDER BY i.created_at DESC LIMIT 50;
			
---------------------------------------------------------
--SQL to list vehicle ratings of a particule vehicle. Multiple tables are combined to get result
---------------------------------------------------------	
SELECT r.*, p.first_name, p.last_name FROM driver_ratings r 
            inner join people p on r.person_id = p.id 
             WHERE r.driver_id = ? ORDER BY r.created_at DESC LIMIT 50;
			
			
---------------------------------------------------------
--SQL to list vehicle trips. Multiple tables are combined to get result
---------------------------------------------------------	
SELECT t.*, pp.first_name, pp.last_name 
             FROM trips t inner join drivers d on t.driver_id = d.id 
            inner join people pp on d.person_id = pp.id 
             WHERE t.vehicle_id = ? ORDER BY t.created_at DESC LIMIT 50;
			
			
---------------------------------------------------------
--SQL to list vehicle ratings. Multiple tables are combined to get result
---------------------------------------------------------	
SELECT vr.*, p.first_name,p.last_name, v.make,v.model,v.year FROM vehicle_ratings vr 
        inner join vehicles v on vr.vehicle_id = v.id inner join people p on vr.person_id = p.id;
		
		
---------------------------------------------------------
--SQL to list vehicles rating. Multiple tables are combined to get result
---------------------------------------------------------	
SELECT v.*, p.first_name,p.last_name, 
(SELECT AVG(rating) from vehicle_ratings where vehicle_id = v.id) average_rating 
FROM vehicles v inner join people p on v.owner_id = p.id;


---------------------------------------------------------
--SQL to list vehicles. Multiple tables are combined to get result
---------------------------------------------------------
SELECT v.*, p.first_name,p.last_name, (SELECT AVG(rating) from 
        vehicle_ratings where vehicle_id = v.id) average_rating 
        FROM vehicles v inner join people p on v.owner_id = p.id;
		

---------------------------------------------------------
-- Other DQL
---------------------------------------------------------		
SELECT COUNT(*) count FROM vehicles;
SELECT COUNT(*) count FROM roads;
SELECT COUNT(*) count FROM drivers;
SELECT COUNT(*) count FROM trips;
SELECT COUNT(*) count FROM passengers;
SELECT COUNT(*) count FROM incidents;
SELECT COUNT(*) count FROM incident_types;
			