
<?php

//This file is used to process request for road ratings
//The request is registered and request parameters are used 
//to refine/filter the searches

require '../_includes/connect.php';

$result = array();
$result['success'] = false;
$personId = $roadId = $comment = $rating = "";



if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

if (isset($_GET['size'])) {
    $recordsPerPage = intval($_GET['size']);
} else {
    $recordsPerPage = 20;
}


$offset = ($page - 1) * $recordsPerPage;

//Sql to count total result to be filed.
$sqlCount = "SELECT COUNT(*) count FROM road_ratings rr inner join roads r on rr.road_id = r.id "
        . "inner join people p on rr.person_id = p.id";
$sqlFetch = "SELECT rr.*, p.first_name,p.last_name,r.name as roadName FROM road_ratings rr "
        . "inner join roads r on rr.road_id = r.id inner join people p on rr.person_id = p.id";
$bindArray = array();

//Get search request parameters
if (isset($_POST['comment'])) {
    $comment = cleanInput($_POST["comment"]);
}
if (isset($_POST['rating1'])) {
    $rating1 = cleanInput($_POST["rating1"]);
}
if (isset($_POST['rating2'])) {
    $rating2 = cleanInput($_POST["rating2"]);
}
if (isset($_POST['person_id'])) {
    $personId = cleanInput($_POST["person_id"]);
}
if (isset($_POST['road_id'])) {
    $roadId = cleanInput($_POST["road_id"]);
}

if (!empty($comment)) {
    $sql = endsWith($sqlCount, "?") ? " AND rr.comment  LIKE ?" : " WHERE rr.comment LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $comment);
}

if (!empty($personId)) {
    $sql = endsWith($sqlCount, "?") ? " AND rr.person_id = ?" : " WHERE rr.person_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $personId);
}

if (!empty($roadId)) {
    $sql = endsWith($sqlCount, "?") ? " AND rr.road_id  = ?" : " WHERE rr.road_id  = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $roadId);
}

if (!empty($rating1)) {
    $sql = endsWith($sqlCount, "?") ? " AND rr.rating >= ?" : " WHERE rr.rating >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $rating1);
}

if (!empty($rating2)) {
    $sql = endsWith($sqlCount, "?") ? " AND rr.rating <= ?" : " WHERE rr.rating <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $rating2);
}

$stmt = $conn->prepare($sqlCount);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

//Execute statement
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

$totalRows = $stmt->get_result()->fetch_assoc()['count'];
$totalPages = ceil($totalRows / $recordsPerPage);

$sqlFetch = $sqlFetch . " LIMIT $offset, $recordsPerPage";

//die($sqlFetch);
$stmt = $conn->prepare($sqlFetch);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}
//die($sqlFetch);
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

// Return Json result
$result['success'] = true;
$result['data'] = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
$result['count'] = count($result['data']);
$result['total'] = $totalRows;
$result['from'] = $offset + 1;
$result['to'] = $offset + $result['count'];
$result['currentPage'] = $page;
$result['totalPages'] = $totalPages;
die(json_encode($result, JSON_NUMERIC_CHECK));
