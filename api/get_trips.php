
<?php

//This file is used to process request for trips
//The request is registered and request parameters are used 
//to refine/filter the searches

require '../_includes/connect.php';

//The result is paginated with 20 records by page. if not page number is 
//supplied, page is assumed to be one.
$result = array();
$result['success'] = false;
$startLocation = $endLocation = $remark = $routeMap = $vehicleId = $driverId = $startTime = $endTime = $maxSpeed = $averageSpeed = $distance = "";



if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

if (isset($_GET['size'])) {
    $recordsPerPage = intval($_GET['size']);
} else {
    $recordsPerPage = 20;
}


$offset = ($page - 1) * $recordsPerPage;

//Sql to count total result to be filed.
$sqlCount = "SELECT COUNT(*) count FROM trips t "
        . "inner join drivers d on t.driver_id = d.id inner join people p on d.person_id = p.id "
        . "inner join vehicles v on t.vehicle_id = v.id";

//Sql to feltch result.
$sqlFetch = "SELECT t.*, d.licence_id, v.make, v.model, v.year, v.registration_number, p.first_name, p.last_name  FROM trips t "
         . "inner join drivers d on t.driver_id = d.id inner join  people p on d.person_id = p.id "
        . "inner join vehicles v on t.vehicle_id = v.id";
$bindArray = array();


//Get search request parameters
if (isset($_POST['start_location'])) {
    $startLocation = cleanInput($_POST["start_location"]);
}
if (isset($_POST['end_location'])) {
    $endLocation = cleanInput($_POST["end_location"]);
}
if (isset($_POST['vehicle_id'])) {
    $vehicleId = cleanInput($_POST["vehicle_id"]);
}
if (isset($_POST['driver_id'])) {
    $driverId = cleanInput($_POST["driver_id"]);
}

if (isset($_POST['end1'])) {
    $endTime1 = cleanInput($_POST["end1"]);
}
if (isset($_POST['end2'])) {
    $endTime2 = cleanInput($_POST["end2"]);
}
if (isset($_POST['start1'])) {
    $startTime1 = cleanInput($_POST["start1"]);
}
if (isset($_POST['start2'])) {
    $startTime2 = cleanInput($_POST["start2"]);
}
if (isset($_POST['max1'])) {
    $maxSpeed1 = cleanInput($_POST["max1"]);
}
if (isset($_POST['max2'])) {
    $maxSpeed2 = cleanInput($_POST["max2"]);
}
if (isset($_POST['distance1'])) {
    $distance1 = cleanInput($_POST["distance1"]);
}
if (isset($_POST['distance2'])) {
    $distance2 = cleanInput($_POST["distance2"]);
}

//Filter result based on search parameters
if (!empty($startLocation)) {
    $sql = endsWith($sqlCount, "?") ? " AND start_location = ?" : " WHERE start_location = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $startLocation);
}

if (!empty($endLocation)) {
    $sql = endsWith($sqlCount, "?") ? " AND end_location = ?" : " WHERE end_location = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $endLocation);
}

if (!empty($driverId)) {
    $sql = endsWith($sqlCount, "?") ? " AND driver_id = ?" : " WHERE driver_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $driverId);
}

if (!empty($vehicleId)) {
    $sql = endsWith($sqlCount, "?") ? " AND vehicle_id = ?" : " WHERE vehicle_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $vehicleId);
}


if (!empty($endTime1)) {
    $sql = endsWith($sqlCount, "?") ? " AND end_time >= ?" : " WHERE end_time >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $endTime1);
}

if (!empty($endTime2)) {
    $sql = endsWith($sqlCount, "?") ? " AND end_time <= ?" : " WHERE end_time <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $endTime2);
}

if (!empty($startTime1)) {
    $sql = endsWith($sqlCount, "?") ? " AND start_time >= ?" : " WHERE start_time >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $startTime1);
}

if (!empty($startTime2)) {
    $sql = endsWith($sqlCount, "?") ? " AND start_time <= ?" : " WHERE start_time <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $startTime2);
}

if (!empty($maxSpeed1)) {
    $sql = endsWith($sqlCount, "?") ? " AND max_speed >= ?" : " WHERE max_speed >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $maxSpeed1);
}

if (!empty($maxSpeed2)) {
    $sql = endsWith($sqlCount, "?") ? " AND max_speed <= ?" : " WHERE max_speed <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $maxSpeed2);
}

if (!empty($distance1)) {
    $sql = endsWith($sqlCount, "?") ? " AND distance_covered >= ?" : " WHERE distance_covered >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $distance1);
}

if (!empty($distance2)) {
    $sql = endsWith($sqlCount, "?") ? " AND distance_covered <= ?" : " WHERE distance_covered <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $distance2);
}

$stmt = $conn->prepare($sqlCount);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

$totalRows = $stmt->get_result()->fetch_assoc()['count'];
$totalPages = ceil($totalRows / $recordsPerPage);

$sqlFetch = $sqlFetch . " LIMIT $offset, $recordsPerPage";

$stmt = $conn->prepare($sqlFetch);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

//Execute statement
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

// Return Json result
$result['success'] = true;
$result['data'] = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
$result['count'] = count($result['data']);
$result['total'] = $totalRows;
$result['from'] = $offset + 1;
$result['to'] = $offset + $result['count'];
$result['currentPage'] = $page;
$result['totalPages'] = $totalPages;
die(json_encode($result, JSON_NUMERIC_CHECK));
