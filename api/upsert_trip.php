
<?php

//This file is used to add new trip or update existing trip
//if the id is null, new record is added to the database, otherwise update is done.

require '../_includes/connect.php';

$startLocation = $endLocation = $remark = $routeMap = $vehicleId = $driverId = $startTime = $endTime = $maxSpeed = $averageSpeed = $distance = "";


$result = array();
$result['success'] = false;

//Verify that request is POST
if ($_SERVER["REQUEST_METHOD"] === "POST") {

      //Get request Parameters
    $id = cleanInput($_POST["selected_id"]);
    $startLocation = cleanInput($_POST["start_location"]);
    $endLocation = cleanInput($_POST["end_location"]);
    $remark = cleanInput($_POST["remark"]);
    $vehicleId = cleanInput($_POST["vehicle_id"]);
    $vehicleId = intval(str_replace("number:", "", $vehicleId));
    $driverId = cleanInput($_POST["driver_id"]);
    $driverId = intval(str_replace("number:", "", $driverId));
    $endTime = cleanInput($_POST["end_time"]);
    $startTime = cleanInput($_POST["start_time"]);
    $maxSpeed = cleanInput($_POST["max_speed"]);
    $averageSpeed = cleanInput($_POST["average_speed"]);
    $distance = cleanInput($_POST["distance_covered"]);

     // Validate required inputs
    if (empty($startLocation) || empty($vehicleId) || empty($startTime) || empty($endTime)) {

        $result['status'] = false;
        $result['message'] = "Some form input is missing, please review and try again";
        die(json_encode($result));
    }

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM drivers WHERE id = ?");
    $stmt->bind_param("s", $driverId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected driver not found";
        die(json_encode($result));
    }

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM vehicles WHERE id = ?");
    $stmt->bind_param("s", $vehicleId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected vehicle not found";
        die(json_encode($result));
    }

       // If ID is null perform insert else perform update
    if (empty($id)) {
        $stmt = $conn->prepare("INSERT INTO trips "
                . "(start_location, end_location, remark, vehicle_id,"
                . " driver_id, start_time, end_time, max_speed, average_speed, "
                . "distance_covered) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?)");

        $stmt->bind_param("ssssssssss", $startLocation, $endLocation, $remark,
                $vehicleId, $driverId, $startTime, $endTime, $maxSpeed, $averageSpeed, $distance);

        if (!$stmt->execute()) {
            $result['success'] = false;
            $result['message'] = $stmt->error;
            die(json_encode($result));
        }

        $last_id = $conn->insert_id;
    } else {

        $stmt = $conn->prepare("UPDATE trips SET start_location = ?, end_location = ?, remark = ?,"
                . " vehicle_id = ?, driver_id = ?, start_time = ?,end_time = ?,"
                . "max_speed = ?,average_speed=?,distance_covered=?  WHERE id = ?");

        $stmt->bind_param("sssssssssss", $startLocation, $endLocation, $remark,
                $vehicleId, $driverId, $startTime, $endTime, $maxSpeed, $averageSpeed, $distance, $id);

        if (!$stmt->execute()) {
            $result['success'] = false;
            $result['message'] = $stmt->error;
            die(json_encode($result));
        }

        $last_id = $id;
    }


// upload the trip map file and add the file name to the object in the DB
    if (!empty($_FILES["route_map"]) && !empty($_FILES["route_map"]["tmp_name"])) {

        $target_dir = "../uploads/";
        $fileName = time() . basename($_FILES["route_map"]["name"]);
        $target_file = $target_dir . $fileName;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


        move_uploaded_file($_FILES["route_map"]["tmp_name"], $target_file);

        $stmt = $conn->prepare("UPDATE trips SET route_map = ? WHERE id = ?");
        $stmt->bind_param("ss", $fileName, $last_id);
        $stmt->execute();
    }

     // Return Json response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        