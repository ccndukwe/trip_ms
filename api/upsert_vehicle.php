
<?php

//This file is used to add new vehicle or update existing vehicle
//if the id is null, new record is added to the database, otherwise update is done.

require '../_includes/connect.php';

$category = $make = $model = $regNumber = $regDate = $ownerId = $year = "";

$result = array();
$result['success'] = false;

//Verify that request is POST
if ($_SERVER["REQUEST_METHOD"] === "POST") {

      //Get request Parameters
    $id = cleanInput($_POST["selected_id"]);
    $category = cleanInput($_POST["category"]);
    $make = cleanInput($_POST["make"]);
    $model = cleanInput($_POST["model"]);
    $regNumber = cleanInput($_POST["reg_number"]);
    $regDate = cleanInput($_POST["reg_date"]);
    $ownerId = cleanInput($_POST["owner_id"]);
    $ownerId = intval(str_replace("number:", "", $ownerId));
    $year = cleanInput($_POST["year"]);
    $year = intval(str_replace("number:", "", $year));

     // Validate required inputs
    if (empty($category) || empty($make) || empty($model) ||
            empty($regNumber) || empty($regDate) || empty($ownerId) ||
            empty($year)) {

        $result['status'] = false;
        $result['message'] = "Some form input is missing, please review and try again";
        die(json_encode($result));
    }

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM people WHERE id = ?");
    $stmt->bind_param("s", $ownerId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected owner not found";
        die(json_encode($result));
    }

       // If ID is null perform insert else perform update
    if (empty($id)) {
        $stmt = $conn->prepare("INSERT INTO vehicles "
                . "(category, make, model, registration_number,"
                . " registration_date, owner_id, year) VALUES (?, ?, ?, ?, ?, ?,?)");

        $stmt->bind_param("sssssss", $category, $make, $model, $regNumber, $regDate,
                $ownerId, $year);
        if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }
    $last_id = $conn->insert_id;
    } else {

        $stmt = $conn->prepare("UPDATE vehicles SET category = ?, make = ?, model = ?, registration_number = ?,"
                . " registration_date = ?, owner_id = ? , year = ? WHERE id = ?");

        $stmt->bind_param("ssssssss", $category, $make, $model, $regNumber, $regDate,
                $ownerId, $year, $id);
        if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }
    $last_id = $id;
    }

    
    if (!empty($_FILES["picture"]) && !empty($_FILES["picture"]["tmp_name"])) {

        $target_dir = "../uploads/";
        $fileName = time() . basename($_FILES["picture"]["name"]);
        $target_file = $target_dir . $fileName;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        
        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["picture"]["tmp_name"]);
        if ($check !== false) {
            move_uploaded_file($_FILES["picture"]["tmp_name"], $target_file);
            $stmt = $conn->prepare("UPDATE vehicles SET picture = ? WHERE id = ?");
            $stmt->bind_param("ss", $fileName, $last_id);
            $stmt->execute();
        }
    }

     // Return Json response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        