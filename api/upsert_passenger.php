
<?php

//This file is used to add new incident types or update existing incident types.
//if the id is null, new record is added to the database, otherwise update is done.


require '../_includes/connect.php';

$personId = $tripId = $seatNumber = "";


$result = array();
$result['success'] = false;

//Verify that request is POST
if ($_SERVER["REQUEST_METHOD"] === "POST") {

      //Get request Parameters
    $id = cleanInput($_POST["selected_id"]);
    $seatNumber = cleanInput($_POST["seat_number"]);
    $tripId = cleanInput($_POST["trip_id"]);
    $tripId = intval(str_replace("number:", "", $tripId));
    $personId = cleanInput($_POST["person_id"]);
    $personId = intval(str_replace("number:", "", $personId));

     // Validate required inputs
    if (empty($seatNumber) || empty($tripId) || empty($personId)) {

        $result['status'] = false;
        $result['message'] = "Some form input is missing, please review and try again";
        die(json_encode($result));
    }

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM people WHERE id = ?");
    $stmt->bind_param("s", $personId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected person not found";
        die(json_encode($result));
    }

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM trips WHERE id = ?");
    $stmt->bind_param("s", $tripId);
    
    
    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected trip not found";
        die(json_encode($result));
    }

       // If ID is null perform insert else perform update
    if (empty($id)) {
        $stmt = $conn->prepare("INSERT INTO passengers "
                . "(seat_number, person_id, trip_id) VALUES (?, ?, ?)");

        $stmt->bind_param("sss", $seatNumber, $personId, $tripId);
    } else {

        $stmt = $conn->prepare("UPDATE passengers SET seat_number = ?, "
                . "person_id = ?, trip_id = ?  WHERE id = ?");

        $stmt->bind_param("ssss", $seatNumber, $personId, $tripId, $id);
    }

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    // Return Json response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        