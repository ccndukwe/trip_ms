
<?php

//The file is used to delete incidents. It takes the incident id and delete boolean.

require '../_includes/connect.php';

$delete = $id = "";

$result = array();
$result['success'] = false;

//Check if request method is post
if ($_SERVER["REQUEST_METHOD"] === "POST") {

     //Get request parameters.
    $id = cleanInput($_POST["selected_id"]);
    $delete = cleanInput($_POST["delete"]);

    if (empty($id) || $delete != 1) {

        $result['status'] = false;
        $result['message'] = "Invalid input, please review and try again";
        die(json_encode($result));
    }

    $stmt = $conn->prepare("DELETE FROM trips WHERE id = ?");
    $stmt->bind_param("s", $id);

//Execute prepared statement
    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }
    
      //Return result
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));  // Return Json Response
}
        