
<?php

//This file is used to process request for road details. 
//The detail includes road details, incidents,
// and ratings.

require '../_includes/connect.php';

$id = "";

if (isset($_GET['id'])) {
    $id = cleanInput($_GET["id"]);

      //SQL to get the road information and average ratings. 
    //Multiple tables are combined to get result.
    $stmt = $conn->prepare("SELECT r.*, (SELECT AVG(rating) from road_ratings where road_id = r.id) average_rating FROM roads r WHERE id = ?");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $road = $stmt->get_result()->fetch_assoc();

         //SQL to get the road incidents 
    //Multiple tables are combined to get result.
    $stmt = $conn->prepare("SELECT i.*, r.name as roadName,it.name as incidentName, v.registration_number, "
            . "t.start_location, t.start_time, pp.first_name, pp.last_name FROM incidents i "
            . "inner join roads r on i.road_id = r.id inner join trips t on i.trip_id = t.id "
            . "inner join incident_types it on i.incident_type_id = it.id "
            . "inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id "
            . "inner join vehicles v on t.vehicle_id = v.id WHERE i.road_id = ? ORDER BY i.created_at DESC LIMIT 50");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $incidents = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    
  //SQL to get the road ratings 
    //Multiple tables are combined to get result.
    $stmt = $conn->prepare("SELECT r.*, p.first_name, p.last_name FROM road_ratings r "
            . "inner join people p on r.person_id = p.id "
            . " WHERE r.road_id = ? ORDER BY r.created_at DESC LIMIT 50");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $ratings = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
}
?>

<div>
    <!-- The detail is returned as a bootstrap modal -->
    <div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="detailTitle">
                        Road Details and Incidents
                        <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                   
                    <div class="row">

                        <div class="col-sm">
                            <b>Name:</b>  <?php echo $road['name'] ?>
                        </div>
                        <div class="col-sm">
                            <b>Class:</b> <?php echo $road['road_class'] ?>
                        </div>

                        <div class="col-sm">
                            <b>Distance:</b> <?php echo $road['distance'] ?>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-sm">
                            <b>Start Town:</b> <?php echo $road['start_town'] ?>
                        </div>

                        <div class="col-sm">
                            <b>End Town:</b> <?php echo $road['end_town'] ?>
                        </div>
                        <div class="col-sm">
                            <b>Average Speed Limit:</b> <?php echo $road['speed_limit'] ?>
                        </div>
                        <div class="col-sm">
                            <b>Average Rating:</b> <?php echo round($road['average_rating'],2) ?>
                        </div>

                    </div>
                    
                    <hr/>
                    <p><b>Remark:</b><br/>
                        <?php echo $road['remark'] ?>
                    </p>
                    <div>
                        <hr/>
                        <h5 class="font-weight-bold">Incidents</h5>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm small">
                                <thead class="bg-dark-blue">
                                    <tr>
                                        <th>Incident</th>
                                        <th>Incident Time</th>
                                        <th>Driver</th>
                                        <th>Remark</th>
                                        <th>Recorded Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($incidents as $incident) { ?>
                                        <tr>
                                            <th><?php echo $incident['incidentName'] ?></th>
                                            <th><?php echo $incident['incident_time'] ?></th>
                                            <th><?php echo $incident['first_name'].' '.$incident['last_name'] ?></th>
                                            <th><?php echo $incident['remark'] ?></th>
                                            <th><?php echo $incident['created_at'] ?></th>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                     <div>
                        <hr/>
                        <h5 class="font-weight-bold">Road Ratings</h5>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm small">
                                <thead class="bg-dark-blue">
                                    <tr>
                                        <th class="text-center">Rating</th>
                                        <th>Comment</th>
                                        <th>Person</th>
                                        <th>Recorded Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($ratings as $rating) { ?>
                                        <tr>
                                            <th class="text-center"><?php echo $rating['rating'] ?></th>
                                            <th><?php echo $rating['rating_comment'] ?></th>
                                            <th><?php echo $rating['first_name'].' '.$rating['last_name'] ?></th>
                                            <th><?php echo $rating['created_at'] ?></th>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </div>
        </div>
    </div>
</div>
</div>
