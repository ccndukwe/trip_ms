
<?php


//This file is used to process request for driver ratings 
//The request parameters are used to refine/filter the searches.

require '../_includes/connect.php';

$result = array();
$result['success'] = false;
$personId = $driverId = $comment = $rating = "";


//The result is paginated with 20 records by page. if not page number is 
//supplied, page is assumed to be one.
if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

if (isset($_GET['size'])) {
    $recordsPerPage = intval($_GET['size']);
} else {
    $recordsPerPage = 20;
}


$offset = ($page - 1) * $recordsPerPage;

//Sql to count total result to be filed.
$sqlCount = "SELECT COUNT(*) count FROM driver_ratings dr inner join drivers d on dr.driver_id = d.id "
        . "inner join people p on dr.person_id = p.id inner join people pd on d.person_id = pd.id";

//Sql to feltch result.
$sqlFetch = "SELECT dr.*, p.first_name,p.last_name, pd.first_name as driverFirstName, "
        . "pd.last_name as driverLastName FROM driver_ratings dr "
        . "inner join drivers d on dr.driver_id = d.id inner join people p on "
        . "dr.person_id = p.id inner join people pd on d.person_id = pd.id";
$bindArray = array();

//Get search inputs
if (isset($_POST['comment'])) {
    $comment = cleanInput($_POST["comment"]);
}
if (isset($_POST['rating1'])) {
    $rating1 = cleanInput($_POST["rating1"]);
}
if (isset($_POST['rating2'])) {
    $rating2 = cleanInput($_POST["rating2"]);
}
if (isset($_POST['person_id'])) {
    $personId = cleanInput($_POST["person_id"]);
}
if (isset($_POST['driver_id'])) {
    $driverId = cleanInput($_POST["driver_id"]);
}

//Filter result based on search inputs
if (!empty($comment)) {
    $sql = endsWith($sqlCount, "?") ? " AND dr.comment  LIKE ?" : " WHERE dr.comment LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $comment);
}

if (!empty($personId)) {
    $sql = endsWith($sqlCount, "?") ? " AND dr.person_id = ?" : " WHERE dr.person_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $personId);
}

if (!empty($driverId)) {
    $sql = endsWith($sqlCount, "?") ? " AND dr.driver_id  = ?" : " WHERE dr.driver_id  = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $driverId);
}

if (!empty($rating1)) {
    $sql = endsWith($sqlCount, "?") ? " AND dr.rating >= ?" : " WHERE dr.rating >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $rating1);
}

if (!empty($rating2)) {
    $sql = endsWith($sqlCount, "?") ? " AND dr.rating <= ?" : " WHERE dr.rating <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $rating2);
}

$stmt = $conn->prepare($sqlCount);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

//Execute statement
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

$totalRows = $stmt->get_result()->fetch_assoc()['count'];
$totalPages = ceil($totalRows / $recordsPerPage);

$sqlFetch = $sqlFetch . " LIMIT $offset, $recordsPerPage";


$stmt = $conn->prepare($sqlFetch);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

// Return Json result
$result['success'] = true;
$result['data'] = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
$result['count'] = count($result['data']);
$result['total'] = $totalRows;
$result['from'] = $offset + 1;
$result['to'] = $offset + $result['count'];
$result['currentPage'] = $page;
$result['totalPages'] = $totalPages;
die(json_encode($result, JSON_NUMERIC_CHECK));
