
<?php


//This file is used to process request for vehicle details. 
//The detail includes name, ownership, ratings and incidents.

require '../_includes/connect.php';

$id = "";

if (isset($_GET['id'])) {
    $id = cleanInput($_GET["id"]);

     //SQL to get the vehicle details 
    //Multiple tables are combined to get other associated fields from other tables.
    $stmt = $conn->prepare("SELECT v.*, p.first_name, p.last_name, "
            . "(SELECT AVG(rating) from vehicle_ratings where vehicle_id = v.id) average_rating "
            . "FROM vehicles v inner join people p on v.owner_id = p.id WHERE v.id = ?");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $vehicle = $stmt->get_result()->fetch_assoc();

    
     //SQL to get the incidents associated to the vehicle 
    //Multiple tables are combined to get other associated fields from other tables.
    $stmt = $conn->prepare("SELECT i.*, r.name as roadName,it.name as incidentName, v.registration_number, "
            . "t.start_location, t.start_time, pp.first_name, pp.last_name FROM incidents i "
            . "inner join roads r on i.road_id = r.id inner join trips t on i.trip_id = t.id "
            . "inner join incident_types it on i.incident_type_id = it.id "
            . "inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id "
            . "inner join vehicles v on t.vehicle_id = v.id WHERE t.vehicle_id = ? ORDER BY i.created_at DESC LIMIT 50");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $incidents = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);

    
     //SQL to get the vehicle ratings 
    //Multiple tables are combined to get other associated fields from other tables.
    $stmt = $conn->prepare("SELECT r.*, p.first_name, p.last_name FROM driver_ratings r "
            . "inner join people p on r.person_id = p.id "
            . " WHERE r.driver_id = ? ORDER BY r.created_at DESC LIMIT 50");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $ratings = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);

    
     //SQL to get the vehicle trips 
    //Multiple tables are combined to get other associated fields from other tables.
    $stmt = $conn->prepare("SELECT t.*, pp.first_name, pp.last_name "
            . " FROM trips t inner join drivers d on t.driver_id = d.id "
            . "inner join people pp on d.person_id = pp.id "
            . " WHERE t.vehicle_id = ? ORDER BY t.created_at DESC LIMIT 50");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $trips = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
}
?>

<div>
    <!-- The detail is returned as a bootstrap modal -->
    <div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="detailTitle">
                        Vehicle Details
                        <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-3 text-center">
                            <img alt="" width="165" src="uploads/<?php echo $vehicle['picture'] ?>" />
                        </div>
                        <div class="col-sm">

                            <div class="row">

                                <div class="col-sm">
                                    <b>Owner:</b>  <?php echo $vehicle['first_name'] . " " . $vehicle['last_name'] ?>
                                </div>
                                <div class="col-sm">
                                    <b>Make:</b> <?php echo $vehicle['make'] ?>
                                </div>

                                <div class="col-sm">
                                    <b>Model:</b> <?php echo $vehicle['model'] ?>
                                </div>
                                <div class="col-sm">
                                    <b>Year:</b> <?php echo $vehicle['year'] ?>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-sm">
                                    <b>Registration Number:</b> <?php echo $vehicle['registration_number'] ?>
                                </div>

                                <div class="col-sm">
                                    <b>Registered Date:</b> <?php echo $vehicle['registration_date'] ?>
                                </div>
                                <div class="col-sm">
                                    <b>Average Rating:</b> <?php echo round($vehicle['average_rating'], 2) ?>
                                </div>

                            </div>
                        </div>
                    </div>

                    <hr/>
                    <h5 class="font-weight-bold">TRIPS</h5>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm small">
                            <thead class="bg-dark-blue">
                                <tr>
                                    <th>Driver</th>
                                    <th>Start Time</th>
                                    <th>Start Location</th>
                                    <th>End Time</th>
                                    <th>End Location </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($trips as $trip) { ?>
                                    <tr>
                                        <th><?php echo $trip['first_name'] . " " . $trip['last_name'] ?></th>
                                        <th><?php echo $trip['start_time'] ?></th>
                                        <th><?php echo $trip['start_location'] ?></th>
                                        <th><?php echo $trip['end_time'] ?></th>
                                        <th><?php echo $trip['end_location'] ?></th>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>


                    <hr/>
                    
                    <div>
                        <hr/>
                        <h5 class="font-weight-bold">Incidents</h5>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm small">
                                <thead class="bg-dark-blue">
                                    <tr>
                                        <th>Incident</th>
                                        <th>Incident Time</th>
                                        <th>Driver</th>
                                        <th>Road</th>
                                        <th>Remark</th>
                                        <th>Recorded Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($incidents as $incident) { ?>
                                        <tr>
                                            <th><?php echo $incident['incidentName'] ?></th>
                                            <th><?php echo $incident['incident_time'] ?></th>
                                            <th><?php echo $incident['first_name'] . ' ' . $incident['last_name'] ?></th>
                                             <th><?php echo $incident['roadName'] ?></th>
                                            <th><?php echo $incident['remark'] ?></th>
                                            <th><?php echo $incident['created_at'] ?></th>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div>
                        <hr/>
                        <h5 class="font-weight-bold">Vehicle Ratings</h5>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm small">
                                <thead class="bg-dark-blue">
                                    <tr>
                                        <th class="text-center">Rating</th>
                                        <th>Comment</th>
                                        <th>Person</th>
                                        <th>Recorded Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($ratings as $rating) { ?>
                                        <tr>
                                            <th class="text-center"><?php echo $rating['rating'] ?></th>
                                            <th><?php echo $rating['rating_comment'] ?></th>
                                            <th><?php echo $rating['first_name'] . ' ' . $rating['last_name'] ?></th>
                                            <th><?php echo $rating['created_at'] ?></th>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </div>
        </div>
    </div>
</div>
</div>
