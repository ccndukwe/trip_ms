
<?php

//The file is used to delete driver. It takes the driver id and delete boolean.

require '../_includes/connect.php'; //Database connection

$delete = $id = "";

$result = array(); //initialize array for returning response
$result['success'] = false;

//Check if request method is post
if ($_SERVER["REQUEST_METHOD"] === "POST") {

   //Get request parameters.
    $id = cleanInput($_POST["selected_id"]);
    $delete = cleanInput($_POST["delete"]);

    if (empty($id) || $delete != 1) { 
    //If there is no id or delete is not true, return an error message.

        $result['status'] = false;
        $result['message'] = "Invalid input, please review and try again";
        die(json_encode($result));
    }

    //Prepare the delete statement ? indicates placeholders.
    $stmt = $conn->prepare("DELETE FROM drivers WHERE id = ?"); // Delete SQL
    $stmt->bind_param("s", $id);

//Execute prepared statement
    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }
    
    // Return Json Response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        