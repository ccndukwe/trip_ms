
<?php

//This file is used to process request for passengers 
//The request parameters are used to refine/filter the searches

require '../_includes/connect.php';

$result = array();
$result['success'] = false;
$personId = $tripId = $seatNumber = "";

//The result is paginated with 20 records by page. if not page number is 
//supplied, page is assumed to be one.
if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

if (isset($_GET['size'])) {
    $recordsPerPage = intval($_GET['size']);
} else {
    $recordsPerPage = 20;
}


$offset = ($page - 1) * $recordsPerPage;

//Sql to count total result to be filed.
$sqlCount = "SELECT COUNT(*) count FROM passengers pg "
        . "inner join trips tp on pg.trip_id = tp.id "
        . "inner join people pp on pg.person_id = pp.id";

//Sql to feltch result.
$sqlFetch = "SELECT pg.*, tp.start_location,tp.end_location, tp.start_time,tp.end_time, "
        . "pp.first_name, pp.last_name  FROM passengers pg "
        . "inner join trips tp on pg.trip_id = tp.id "
        . "inner join people pp on pg.person_id = pp.id";
$bindArray = array();

//Get search request parameters
if (isset($_POST['seat_number'])) {
    $seatNumber = cleanInput($_POST["seat_number"]);
}

if (isset($_POST['trip_id'])) {
    $tripId = cleanInput($_POST["trip_id"]);
}
if (isset($_POST['person_id'])) {
    $personId = cleanInput($_POST["person_id"]);
}

//Filter result based on search parameters
if (!empty($seatNumber)) {
    $sql = endsWith($sqlCount, "?") ? " AND seat_number = ?" : " WHERE seat_number = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $seatNumber);
}

if (!empty($tripId)) {
    $sql = endsWith($sqlCount, "?") ? " AND trip_id = ?" : " WHERE trip_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $tripId);
}

if (!empty($personId)) {
    $sql = endsWith($sqlCount, "?") ? " AND person_id = ?" : " WHERE person_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $personId);
}


$stmt = $conn->prepare($sqlCount);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

//Execute statement
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

$totalRows = $stmt->get_result()->fetch_assoc()['count'];
$totalPages = ceil($totalRows / $recordsPerPage);

$sqlFetch = $sqlFetch . " LIMIT $offset, $recordsPerPage";

$stmt = $conn->prepare($sqlFetch);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

// Return Json result
$result['success'] = true;
$result['data'] = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
$result['count'] = count($result['data']);
$result['total'] = $totalRows;
$result['from'] = $offset + 1;
$result['to'] = $offset + $result['count'];
$result['currentPage'] = $page;
$result['totalPages'] = $totalPages;
die(json_encode($result, JSON_NUMERIC_CHECK));
