
<?php

//This php file is used to delete a driver rating

require '../_includes/connect.php'; //Database connection

$delete = $id = "";

$result = array(); //initialize array for returning response
$result['success'] = false;

//Check if request method is post
if ($_SERVER["REQUEST_METHOD"] === "POST") {

    //Get request parameters.
    $id = cleanInput($_POST["selected_id"]);
    $delete = cleanInput($_POST["delete"]);

    //Check if parameter exists and not null
    if (empty($id) || $delete != 1) {

        $result['status'] = false;
        $result['message'] = "Invalid input, please review and try again";
        die(json_encode($result));
    }

    //Prepare the delete statement ? indicates placeholders.
    $stmt = $conn->prepare("DELETE FROM driver_ratings WHERE id = ?"); // Delete SQL
    $stmt->bind_param("s", $id);


    //Execute prepared statement
    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }
    
    //Return result
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result)); // Return Json Response
}
        