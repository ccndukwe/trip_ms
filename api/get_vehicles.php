
<?php

//This file is used to process request for vehicles
//The request is registered and request parameters are used 
//to refine/filter the searches

require '../_includes/connect.php';

//The result is paginated with 20 records by page. if not page number is 
//supplied, page is assumed to be one.
$result = array();
$result['success'] = false;
$category = $make = $model = $regNumber = $regDate = $ownerId = $year = "";
    
if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

if (isset($_GET['size'])) {
    $recordsPerPage = intval($_GET['size']);
} else {
    $recordsPerPage = 20;
}


$offset = ($page - 1) * $recordsPerPage;

//Sql to count total result to be filed.
$sqlCount = "SELECT COUNT(*) count FROM vehicles ";
$sqlFetch = "SELECT v.*, p.first_name,p.last_name, (SELECT AVG(rating) from "
        . "vehicle_ratings where vehicle_id = v.id) average_rating "
        . "FROM vehicles v inner join people p on v.owner_id = p.id";
$bindArray = array();


//Get search request parameters
if (isset($_POST['category'])) {
    $category = cleanInput($_POST["category"]);
}
if (isset($_POST['make'])) {
    $make = cleanInput($_POST["make"]);
}
if (isset($_POST['model'])) {
    $model = cleanInput($_POST["model"]);
}
if (isset($_POST['reg_number'])) {
    $regNumber = cleanInput($_POST["reg_number"]);
}
if (isset($_POST['owner_id'])) {
    $ownerId = intval(cleanInput($_POST["owner_id"]));
}

if (isset($_POST['year'])) {
    $year = cleanInput($_POST["year"]);
}
if (isset($_POST['date1'])) {
    $regDate1 = cleanInput($_POST["date1"]);
}
if (isset($_POST['date2'])) {
    $regDate2 = cleanInput($_POST["date2"]);
}

//Filter result based on search parameters
if (!empty($category)) {
    $sql = endsWith($sqlCount, "?") ? " AND category = ?" : " WHERE category = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $category);
}

if (!empty($make)) {
    $sql = endsWith($sqlCount, "?") ? " AND make LIKE ?" : " WHERE make LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $make);
}

if (!empty($model)) {
    $sql = endsWith($sqlCount, "?") ? " AND model = ?" : " WHERE model = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $model);
}


if (!empty($regNumber)) {
    $sql = endsWith($sqlCount, "?") ? " AND registration_number = ?" : " WHERE registration_number = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $regNumber);
}

if (!empty($ownerId)) {
    $sql = endsWith($sqlCount, "?") ? " AND owner_id = ?" : " WHERE owner_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $ownerId);
}

if (!empty($year)) {
    $sql = endsWith($sqlCount, "?") ? " AND year = ?" : " WHERE year = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $year);
}

if (!empty($regDate1)) {
    $sql = endsWith($sqlCount, "?") ? " AND registration_date >= ?" : " WHERE registration_date >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $regDate1);
}

if (!empty($regDate2)) {
    $sql = endsWith($sqlCount, "?") ? " AND registration_date <= ?" : " WHERE registration_date <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $regDate2);
}

$stmt = $conn->prepare($sqlCount);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}


if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

$totalRows = $stmt->get_result()->fetch_assoc()['count'];
$totalPages = ceil($totalRows / $recordsPerPage);

$sqlFetch = $sqlFetch . " LIMIT $offset, $recordsPerPage";

$stmt = $conn->prepare($sqlFetch);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

//Execute statement
$result['success'] = true;
$result['data'] = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
$result['count'] = count($result['data']);
$result['total'] = $totalRows;
$result['from'] = $offset + 1;
$result['to'] = $offset + $result['count'];
$result['currentPage'] = $page;
$result['totalPages'] = $totalPages;
die(json_encode($result, JSON_NUMERIC_CHECK));
