
<?php

//This file is used to process request for drivers 
//The request parameters are used to refine/filter the searches.

require '../_includes/connect.php';

$result = array();
$result['success'] = false;
$licenceId = $licenceClass = $personId = $firstName = $lastName = $liceneDate1 
        = $licenceDate2 = $expireDate1 = $expireDate2 = "";


//The result is paginated with 20 records by page. if not page number is 
//supplied, page is assumed to be one.
if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

if (isset($_GET['size'])) {
    $recordsPerPage = intval($_GET['size']);
} else {
    $recordsPerPage = 20;
}


$offset = ($page - 1) * $recordsPerPage;

//Sql to count total result to be filed.
$sqlCount = "SELECT COUNT(*) count FROM drivers d inner join people p on d.person_id = p.id ";

//Sql to feltch result.
$sqlFetch = "SELECT d.*, p.first_name, p.last_name, (SELECT AVG(rating) from driver_ratings where driver_id = d.id) average_rating FROM drivers d inner join people p on d.person_id = p.id";
$bindArray = array();

//Get search request parameters
if (isset($_POST['licence_id'])) {
    $licenceId = cleanInput($_POST["licence_id"]);
}
if (isset($_POST['licence_class'])) {
    $licenceClass = cleanInput($_POST["licence_class"]);
}
if (isset($_POST['person_id'])) {
    $personId = cleanInput($_POST["person_id"]);
}
if (isset($_POST['first_name'])) {
    $firstName = cleanInput($_POST["first_name"]);
}
if (isset($_POST['last_name'])) {
    $lastName = cleanInput($_POST["last_name"]);
}

if (isset($_POST['lDate1'])) {
    $licenseDate1 = cleanInput($_POST["lDate1"]);
}
if (isset($_POST['lDate2'])) {
    $licenseDate2 = cleanInput($_POST["lDate2"]);
}

if (isset($_POST['eDate1'])) {
    $expireDate1 = cleanInput($_POST["eDate1"]);
}
if (isset($_POST['eDate2'])) {
    $expireDate2 = cleanInput($_POST["eDate2"]);
}

//Filter result based on search parameters
if (!empty($licenceId)) {
    $sql = endsWith($sqlCount, "?") ? " AND licence_id = ?" : " WHERE licence_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $licenceId);
}

if (!empty($licenceClass)) {
    $sql = endsWith($sqlCount, "?") ? " AND licence_class = ?" : " WHERE licence_class = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $licenceClass);
}

if (!empty($personId)) {
    $sql = endsWith($sqlCount, "?") ? " AND person_id = ?" : " WHERE person_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $personId);
}


if (!empty($firstName)) {
    $sql = endsWith($sqlCount, "?") ? " AND p.first_name LIKE ?" : " WHERE p.first_name LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $firstName);
}

if (!empty($lastName)) {
    $sql = endsWith($sqlCount, "?") ? " AND p.last_name LIKE ?" : " WHERE p.last_name LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $lastName);
}

if (!empty($licenseDate1)) {
    $sql = endsWith($sqlCount, "?") ? " AND licence_date >= ?" : " WHERE licence_date >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $licenseDate1);
}

if (!empty($licenseDate2)) {
    $sql = endsWith($sqlCount, "?") ? " AND licence_date <= ?" : " WHERE licence_date <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $licenseDate2);
}

if (!empty($expireDate1)) {
    $sql = endsWith($sqlCount, "?") ? " AND licence_expiry_date >= ?" : " WHERE licence_expiry_date >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $expireDate1);
}

if (!empty($expireDate2)) {
    $sql = endsWith($sqlCount, "?") ? " AND licence_expiry_date <= ?" : " WHERE licence_expiry_date <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $expireDate2);
}

$stmt = $conn->prepare($sqlCount);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}


//Execute statement
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

$totalRows = $stmt->get_result()->fetch_assoc()['count'];
$totalPages = ceil($totalRows / $recordsPerPage);

$sqlFetch = $sqlFetch . " LIMIT $offset, $recordsPerPage";

$stmt = $conn->prepare($sqlFetch);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

// Return Json result
$result['success'] = true;
$result['data'] = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
$result['count'] = count($result['data']);
$result['total'] = $totalRows;
$result['from'] = $offset + 1;
$result['to'] = $offset + $result['count'];
$result['currentPage'] = $page;
$result['totalPages'] = $totalPages;
die(json_encode($result, JSON_NUMERIC_CHECK));
