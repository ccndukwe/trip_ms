
<?php

//This file is used to add new road or update existing road.
//if the id is null, new record is added to the database, otherwise update is done.


require '../_includes/connect.php';

$name = $class = $distance = $startTown = $endTown = $speedLimit = $remark = $id = "";

$result = array();
$result['success'] = false;

//Verify that request is POST
if ($_SERVER["REQUEST_METHOD"] === "POST") {

      //Get request Parameters
    $id = cleanInput($_POST["selected_id"]);
    $name = cleanInput($_POST["name"]);
    $class = cleanInput($_POST["class"]);
    $class = str_replace("string:", "", $class);
    $distance = doubleval(cleanInput($_POST["distance"]));
    $startTown = cleanInput($_POST["start_town"]);
    $endTown = cleanInput($_POST["end_town"]);
    $speedLimit = doubleval(cleanInput($_POST["speed_limit"]));
    $remark = cleanInput($_POST["remark"]);

     // Validate required inputs
    if (empty($name) || empty($startTown) || empty($endTown)) {
        $result['status'] = false;
        $result['message'] = "Some form input is missing, please review and try again";
        die(json_encode($result));
    }

       // If ID is null perform insert else perform update
    if (empty($id)) {
        $stmt = $conn->prepare("INSERT INTO roads "
                . "(name, road_class, distance, start_town,"
                . " end_town, speed_limit, remark) VALUES (?, ?, ?, ?, ?, ?,?)");

        $stmt->bind_param("sssssss", $name, $class, $distance, $startTown, $endTown,
                $speedLimit, $remark);
    } else {

        $stmt = $conn->prepare("UPDATE roads SET name = ?, road_class = ?, distance = ?, start_town = ?,"
                . " end_town = ?, speed_limit = ?, remark = ? WHERE id = ?");

        $stmt->bind_param("ssssssss", $name, $class, $distance, $startTown, $endTown,
                $speedLimit, $remark, $id);
    }



    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }


     // Return Json response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        