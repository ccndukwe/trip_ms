
<?php

//This file is used to add new vehicle rating or update existing vehicle rating.
//if the id is null, new record is added to the database, otherwise update is done.


require '../_includes/connect.php';

$personId = $vehicleId = $comment = $rating = "";


$result = array();
$result['success'] = false;

//Verify that request is POST
if ($_SERVER["REQUEST_METHOD"] === "POST") {

    //Get request Parameters
    $id = cleanInput($_POST["selected_id"]);
    $comment = cleanInput($_POST["comment"]);
    $personId = cleanInput($_POST["person_id"]);
    $personId = intval(str_replace("number:", "", $personId));
    $vehicleId = cleanInput($_POST["vehicle_id"]);
    $vehicleId = intval(str_replace("number:", "", $vehicleId));
    $rating = cleanInput($_POST["rating"]);
    $rating = intval(str_replace("number:", "", $rating));

     // Validate required inputs
    if (empty($personId) || empty($vehicleId) || empty($rating)) {

        $result['status'] = false;
        $result['message'] = "Some form input is missing, please review and try again";
        die(json_encode($result));
    }

    // Check if the person making the rating exist
    $stmt = $conn->prepare("SELECT COUNT(*) count FROM people WHERE id = ?");
    $stmt->bind_param("s", $personId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected person not found";
        die(json_encode($result));
    }

     // Check if the driver to be vehicle exists
    $stmt = $conn->prepare("SELECT COUNT(*) count FROM vehicles WHERE id = ?");
    $stmt->bind_param("s", $vehicleId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected vehicle not found";
        die(json_encode($result));
    }
    

 // If ID is null perform insert else perform update
    if (empty($id)) {
        $stmt = $conn->prepare("INSERT INTO vehicle_ratings "
                . "(rating, person_id, vehicle_id,"
                . " rating_comment) VALUES (?, ?, ?, ?)");

        $stmt->bind_param("ssss", $rating, $personId, $vehicleId, $comment);
    } else {

        $stmt = $conn->prepare("UPDATE vehicle_ratings SET rating = ?,"
                . " person_id = ?, vehicle_id = ?, rating_comment = ?  WHERE id = ?");
        
        $stmt->bind_param("sssss", $rating, $personId, $vehicleId, $comment, $id);
    }

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    // Return Json response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        