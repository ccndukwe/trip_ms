
<?php

//This file is used to add new road rating or update existing road rating.
//if the id is null, new record is added to the database, otherwise update is done.


require '../_includes/connect.php';

$personId = $roadId = $comment = $rating = "";


$result = array();
$result['success'] = false;

//Verify that request is POST
if ($_SERVER["REQUEST_METHOD"] === "POST") {

      //Get request Parameters
    $id = cleanInput($_POST["selected_id"]);
    $comment = cleanInput($_POST["comment"]);
    $personId = cleanInput($_POST["person_id"]);
    $personId = intval(str_replace("number:", "", $personId));
    $roadId = cleanInput($_POST["road_id"]);
    $roadId = intval(str_replace("number:", "", $roadId));
    $rating = cleanInput($_POST["rating"]);
    $rating = intval(str_replace("number:", "", $rating));

     // Validate required inputs
    if (empty($personId) || empty($roadId) || empty($rating)) {

        $result['status'] = false;
        $result['message'] = "Some form input is missing, please review and try again";
        die(json_encode($result));
    }

     // Check if the person making the rating exist
    $stmt = $conn->prepare("SELECT COUNT(*) count FROM people WHERE id = ?");
    $stmt->bind_param("s", $personId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected person not found";
        die(json_encode($result));
    }

        // Check if the driver to be road to be rated exists
    $stmt = $conn->prepare("SELECT COUNT(*) count FROM roads WHERE id = ?");
    $stmt->bind_param("s", $roadId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected road not found";
        die(json_encode($result));
    }
    

       // If ID is null perform insert else perform update
    if (empty($id)) {
        $stmt = $conn->prepare("INSERT INTO road_ratings "
                . "(rating, person_id, road_id,"
                . " rating_comment) VALUES (?, ?, ?, ?)");

        $stmt->bind_param("ssss", $rating, $personId, $roadId, $comment);
    } else {

        $stmt = $conn->prepare("UPDATE road_ratings SET rating = ?,"
                . " person_id = ?, road_id = ?, rating_comment = ?  WHERE id = ?");
        
        $stmt->bind_param("sssss", $rating, $personId, $roadId, $comment, $id);
    }

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

     // Return Json response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        