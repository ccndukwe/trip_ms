
<?php

//This file is used to add new driver or update existing driver.
//if the id is null, new record is added to the database, otherwise update is done.


require '../_includes/connect.php';

$category = $make = $model = $regNumber = $regDate = $ownerId = $year = "";

$result = array();
$result['success'] = false;

//Verify that request is POST
if ($_SERVER["REQUEST_METHOD"] === "POST") {

      //Get request Parameters
    $id = cleanInput($_POST["selected_id"]);
    $licenceId = cleanInput($_POST["licence_id"]);
    $licenceClass = cleanInput($_POST["licence_class"]);
    $personId = cleanInput($_POST["person_id"]);
    $personId = intval(str_replace("number:", "", $personId));
    $licenceDate = cleanInput($_POST["licence_date"]);
    $expireDate = cleanInput($_POST["licence_expiry_date"]);

     // Validate required inputs
    if (empty($licenceId) || empty($licenceClass) || empty($personId)) {

        $result['status'] = false;
        $result['message'] = "Some form input is missing, please review and try again";
        die(json_encode($result));
    }

    // Check if the person assoicated with the driver exists
    $stmt = $conn->prepare("SELECT COUNT(*) count FROM people WHERE id = ?");
    $stmt->bind_param("s", $personId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected owner not found";
        die(json_encode($result));
    }

    // If ID is null perform insert else perform update
    if (empty($id)) {
        $stmt = $conn->prepare("INSERT INTO drivers "
                . "(licence_id, licence_class, person_id, licence_date,"
                . " licence_expiry_date) VALUES (?, ?, ?, ?, ?)");

        $stmt->bind_param("sssss", $licenceId, $licenceClass, $personId, $licenceDate, $expireDate);
    } else {

        $stmt = $conn->prepare("UPDATE drivers SET licence_id = ?, licence_class = ?, person_id = ?,"
                . " licence_date = ?, licence_expiry_date = ? WHERE id = ?");

        $stmt->bind_param("ssssss", $licenceId, $licenceClass, $personId, $licenceDate, $expireDate, $id);
    }

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

     // Return Json response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        