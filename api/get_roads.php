
<?php

//This file is used to process request for roads
//The request is registered and request parameters are used 
//to refine/filter the searches

require '../_includes/connect.php';

//The result is paginated with 20 records by page. if not page number is 
//supplied, page is assumed to be one.
$result = array();
$result['success'] = false;
$name = $class = $starTown = $endTown = $limit1 = $limit2 = "";

if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

if (isset($_GET['size'])) {
    $recordsPerPage = intval($_GET['size']);
} else {
    $recordsPerPage = 20;
}


$offset = ($page - 1) * $recordsPerPage;

//Sql to count total result to be filed.
$sqlCount = "SELECT COUNT(*) count FROM roads ";

//Sql to feltch result.
$sqlFetch = "SELECT r.*, (SELECT AVG(rating) from road_ratings where road_id = r.id) average_rating FROM roads r ";
$bindArray = array();

//Get search request parameters
if (isset($_POST['name'])) {
    $name = cleanInput($_POST["name"]);
}
if (isset($_POST['class'])) {
    $class = cleanInput($_POST["class"]);
}
if (isset($_POST['start_town'])) {
    $starTown = cleanInput($_POST["start_town"]);
}
if (isset($_POST['end_town'])) {
    $endTown = cleanInput($_POST["end_town"]);
}
if (isset($_POST['limit1'])) {
    $limit1 = cleanInput($_POST["limit1"]);
}
if (isset($_POST['limit2'])) {
    $limit2 = cleanInput($_POST["limit2"]);
}

//Filter result based on search parameters
if (!empty($name)) {
    $sql = endsWith($sqlCount, "?") ? " AND name LIKE ?" : " WHERE name LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $name);
}

if (!empty($class)) {
    $sql = endsWith($sqlCount, "?") ? " AND road_class = ?" : " WHERE road_class = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $class);
}


if (!empty($starTown)) {
    $sql = endsWith($sqlCount, "?") ? " AND start_town = ?" : " WHERE start_town = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $starTown);
}

if (!empty($endTown)) {
    $sql = endsWith($sqlCount, "?") ? " AND end_town = ?" : " WHERE end_town = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $endTown);
}

if (!empty($limit1)) {
    $sql = endsWith($sqlCount, "?") ? " AND speed_limit >= ?" : " WHERE speed_limit >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $limit1);
}

if (!empty($limit2)) {
    $sql = endsWith($sqlCount, "?") ? " AND speed_limit <= ?" : " WHERE speed_limit <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $limit2);
}

$stmt = $conn->prepare($sqlCount);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

$totalRows = $stmt->get_result()->fetch_assoc()['count'];
$totalPages = ceil($totalRows / $recordsPerPage);

$sqlFetch = $sqlFetch . " LIMIT $offset, $recordsPerPage";

$stmt = $conn->prepare($sqlFetch);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

//Execute statement
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

// Return Json result
$result['success'] = true;
$result['data'] = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
$result['count'] = count($result['data']);
$result['total'] = $totalRows;
$result['from'] = $offset + 1;
$result['to'] = $offset + $result['count'];
$result['currentPage'] = $page;
$result['totalPages'] = $totalPages;
die(json_encode($result, JSON_NUMERIC_CHECK));
