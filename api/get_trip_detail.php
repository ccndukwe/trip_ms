
<?php

//This file is used to process request for trip details. 
//The detail includes passengers, and incidents.

require '../_includes/connect.php';

$id = "";

if (isset($_GET['id'])) {
    $id = cleanInput($_GET["id"]);

         //SQL to get the trip information 
    //Multiple tables are combined to get result.
    $stmt = $conn->prepare("SELECT t.*, d.licence_id, v.make, v.model, v.year, v.registration_number,"
            . " p.first_name as driverFirstName, p.last_name as driverLastName, "
            . " o.first_name as ownerFirstName, o.last_name as ownerLastName FROM trips t "
            . "inner join drivers d on t.driver_id = d.id inner join  people p on d.person_id = p.id "
            . "inner join vehicles v on t.vehicle_id = v.id "
            . "inner join people o on v.owner_id = o.id WHERE t.id = ?");
    $stmt->bind_param('s', $id);
    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $trip = $stmt->get_result()->fetch_assoc();

             //SQL to get the road incidents 
    //Multiple tables are combined to get result.
    $stmt = $conn->prepare("SELECT i.*, r.name as roadName,it.name as incidentName, v.registration_number, "
            . "t.start_location, t.start_time, pp.first_name, pp.last_name FROM incidents i "
            . "inner join roads r on i.road_id = r.id inner join trips t on i.trip_id = t.id "
            . "inner join incident_types it on i.incident_type_id = it.id "
            . "inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id "
            . "inner join vehicles v on t.vehicle_id = v.id WHERE t.id = ? ORDER BY i.created_at DESC LIMIT 50");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $incidents = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);

             //SQL to get the trip passengers
    //Multiple tables are combined to get result.
    $stmt = $conn->prepare("SELECT p.*, pp.first_name, pp.last_name FROM passengers p "
            . "inner join people pp on p.person_id = pp.id "
            . " WHERE p.trip_id = ? ORDER BY pp.first_name");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $passengers = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
}
?>

<div>
    <!-- The detail is returned as a bootstrap modal -->
    <div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="detailTitle">
                        Trip Details 
                        <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">

                    <div class="row">

                        <div class="col-sm">
                            <b>Start Time:</b>  <?php echo $trip['start_time'] ?>
                        </div>
                        <div class="col-sm">
                            <b>Start Town:</b> <?php echo $trip['start_location'] ?>
                        </div>

                        <div class="col-sm">
                            <b>End Time:</b> <?php echo $trip['end_time'] ?>
                        </div>
                        <div class="col-sm">
                            <b>End Town:</b> <?php echo $trip['end_location'] ?>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">

                         <div class="col-sm">
                            <b>Maximum:</b> <?php echo $trip['max_speed'] ?>
                        </div>
                        <div class="col-sm">
                            <b>Average Speed:</b> <?php echo $trip['average_speed'] ?>
                        </div>
                        <div class="col-sm">
                            <b>Distance Covered:</b> <?php echo $trip['distance_covered'] ?>
                        </div>

                    </div>
                    <hr/>
                    <div class="row">

                        <div class="col-sm">
                            <b>Driver Name:</b> <br/><?php echo $trip['driverFirstName']." ".$trip['driverLastName'] ?>
                        </div>
                        <div class="col-sm">
                            <b>Driver License Id:</b> <br/><?php echo $trip['licence_id'] ?>
                        </div>
                        <div class="col-sm">
                            <b>Vehicle Owner:</b><br/><?php echo $trip['ownerFirstName']." ".$trip['ownerLastName'] ?>
                        </div>
                        <div class="col-sm">
                            <b>Vehicle Registration Id:</b><br/> <?php echo $trip['registration_number'] ?>
                        </div>


                    </div>
                    <hr/>
                    <p><b>Remark:</b><br/>
                        <?php echo $trip['remark'] ?>
                    </p>
                    <?php if ($trip['route_map']) { ?>
                        <p>
                            <a target="_blank" href="uploads/<?php echo $trip['route_map'] ?>">View trip map</a>

                        </p>
                    <?php } ?>
                    <div>
                        <hr/>
                        <h5 class="font-weight-bold">Incidents</h5>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm small">
                                <thead class="bg-dark-blue">
                                    <tr>
                                        <th>Incident</th>
                                        <th>Incident Time</th>
                                        <th>Driver</th>
                                        <th>Road</th>
                                        <th>Remark</th>
                                        <th>Recorded Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($incidents as $incident) { ?>
                                        <tr>
                                            <th><?php echo $incident['incidentName'] ?></th>
                                            <th><?php echo $incident['incident_time'] ?></th>
                                            <th><?php echo $incident['first_name'] . ' ' . $incident['last_name'] ?></th>
                                             <th><?php echo $incident['roadName'] ?></th>
                                             <th><?php echo $incident['remark'] ?></th>
                                            <th><?php echo $incident['created_at'] ?></th>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div>
                        <hr/>
                        <h5 class="font-weight-bold">Passengers</h5>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm small">
                                <thead class="bg-dark-blue">
                                    <tr>
                                        <th>Name</th>
                                        <th>Seat Number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($passengers as $passenger) { ?>
                                        <tr>
                                            <th><?php echo $passenger['first_name'] . ' ' . $passenger['last_name'] ?></th>
                                            <th><?php echo $passenger['seat_number'] ?></th>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </div>
        </div>
    </div>
</div>
</div>
