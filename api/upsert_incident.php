
<?php

//This file is used to add new incident or update existing incident.
//if the id is null, new record is added to the database, otherwise update is done.

require '../_includes/connect.php';

$tripId = $roadId = $remark = $typeId = $incidentType = "";


$result = array();
$result['success'] = false;

//Verify that request is POST
if ($_SERVER["REQUEST_METHOD"] === "POST") {

      //Get request Parameters
    $id = cleanInput($_POST["selected_id"]);
    $remark = cleanInput($_POST["remark"]);
    $tripId = cleanInput($_POST["trip_id"]);
    $tripId = intval(str_replace("number:", "", $tripId));
    $roadId = cleanInput($_POST["road_id"]);
    $roadId = intval(str_replace("number:", "", $roadId));
    $typeId = cleanInput($_POST["incident_type_id"]);
    $typeId = intval(str_replace("number:", "", $typeId));
    $incidentTime = cleanInput($_POST["incident_time"]);

     // Validate required inputs
    if (empty($roadId) || empty($tripId) || empty($typeId) || empty($incidentTime)) {

        $result['status'] = false;
        $result['message'] = "Some form input is missing, please review and try again";
        die(json_encode($result));
    }

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM trips WHERE id = ?");
    $stmt->bind_param("s", $tripId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected trip not found";
        die(json_encode($result));
    }

    $stmt = $conn->prepare("SELECT COUNT(*) count FROM roads WHERE id = ?");
    $stmt->bind_param("s", $roadId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected road not found";
        die(json_encode($result));
    }
    
    $stmt = $conn->prepare("SELECT COUNT(*) count FROM incident_types WHERE id = ?");
    $stmt->bind_param("s", $typeId);

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

    $resultCount = $stmt->get_result()->fetch_assoc()['count'];

    if ($resultCount != 1) {
        $result['success'] = false;
        $result['message'] = "Selected incident_type not found";
        die(json_encode($result));
    }

       // If ID is null perform insert else perform updates
    if (empty($id)) {
        $stmt = $conn->prepare("INSERT INTO incidents "
                . "(incident_time, remark, road_id,"
                . " trip_id, incident_type_id) VALUES (?, ?, ?, ?, ?)");

        $stmt->bind_param("sssss", $incidentTime, $remark, $roadId,
                $tripId, $typeId);
    } else {

        $stmt = $conn->prepare("UPDATE incidents SET incident_time = ?, remark = ?,"
                . " road_id = ?, trip_id = ?, incident_type_id = ?  WHERE id = ?");
        
        $stmt->bind_param("ssssss", $incidentTime, $remark, $roadId,$tripId, $typeId, $id);
    }

    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

 // Return Json response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        