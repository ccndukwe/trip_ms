
<?php

//This file is used to add new incident types or update existing incident types.
//if the id is null, new record is added to the database, otherwise update is done.

require '../_includes/connect.php';

$name = $description = $id = "";

$result = array();
$result['success'] = false;

//Verify that request is POST
if ($_SERVER["REQUEST_METHOD"] === "POST") {

      //Get request Parameters
    $id = cleanInput($_POST["selected_id"]);
    $name = cleanInput($_POST["name"]);
    $description = cleanInput($_POST["description"]);

     // Validate required inputs
    if (empty($name)) {
        $result['status'] = false;
        $result['message'] = "Some form input is missing, please review and try again";
        die(json_encode($result));
    }

       // If ID is null perform insert else perform update
    if (empty($id)) {
        $stmt = $conn->prepare("INSERT INTO incident_types "
                . "(name, description) VALUES (?, ?)");

        $stmt->bind_param("ss", $name,$description);
    } else {

        $stmt = $conn->prepare("UPDATE incident_types SET name = ?, description = ? WHERE id = ?");

        $stmt->bind_param("sss", $name, $description, $id);
    }



    if (!$stmt->execute()) {
        $result['success'] = false;
        $result['message'] = $stmt->error;
        die(json_encode($result));
    }

 // Return Json response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        