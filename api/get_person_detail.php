
<?php


//This file is used to process request for person details. 
//The detail includes personal details of the person, trips taken,
// and vehicles owned.


require '../_includes/connect.php';

$id = "";

if (isset($_GET['id'])) {
    $id = cleanInput($_GET["id"]);

     //SQL to get the person information from people table.
    $stmt = $conn->prepare("SELECT * FROM people WHERE id = ?");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $person = $stmt->get_result()->fetch_assoc();

     //SQL to get trips taken by the person.
    //Multiple tables are combined to get result.s
    $stmt = $conn->prepare("SELECT p.*, t.start_time, t.start_location, t.end_time, "
            . "t.end_location, pp.first_name, pp.last_name, v.registration_number "
            . " FROM passengers p inner join trips t on p.trip_id = t.id "
            . "inner join drivers d on t.driver_id = d.id "
            . "inner join people pp on d.person_id = pp.id "
            . "inner join vehicles v on t.vehicle_id = v.id "
            . " WHERE p.person_id = ? ORDER BY p.created_at DESC LIMIT 50");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }

    $trips = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    
     //SQL to get the vehicles owned by the person. 
    //Multiple tables are combined to get more fields.
    $stmt = $conn->prepare("SELECT v.*, (SELECT AVG(rating) from vehicle_ratings where vehicle_id = v.id) average_rating FROM vehicles v WHERE v.owner_id = ?");
    $stmt->bind_param('s', $id);

    if (!$stmt->execute()) {
        die(json_encode($stmt->error));
    }
    $vehicles = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
}
?>

<div>
    <!-- The detail is returned as a bootstrap modal -->
    <div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="detailTitle">
                        Person Details and Trips
                        <i ng-show="processing" class="fa fa-spinner fa-2x fa-spin"></i>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <p class="text-center">

                    </p>
                    <div class="row">
                        <div class="col-sm-3 text-center">
                            <img alt="" width="165" src="uploads/<?php echo $person['picture'] ?>" />
                        </div>
                        <div class="col-sm">
                            <div class="row">
                                <div class="col-sm">
                                    <b>First Name:</b>  <?php echo $person['first_name'] ?>
                                </div>
                                <div class="col-sm">
                                    <b>Last Name:</b> <?php echo $person['last_name'] ?>
                                </div>

                                <div class="col-sm">
                                    <b>Date of Birth:</b> <?php echo $person['birth_date'] ?>
                                </div>
                                <div class="col-sm">
                                    <b>Email:</b> <?php echo $person['email'] ?>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-sm">
                                    <b>Registration Id:</b>  <?php echo $person['registration_id'] ?>
                                </div>
                                <div class="col-sm">
                                    <b>Gender:</b> <?php echo $person['gender'] ?>
                                </div>
                                <div class="col-sm">
                                    <b>Address:</b> <?php
                                    echo $person['address_number']
                                    . " " . $person['address_street']
                                    . ", " . $person['address_post_code']
                                    . ", " . $person['address_town']
                                    ?>
                                </div>

                            </div>
                        </div>
                    </div>

                    <hr/>
                    <h5 class="font-weight-bold">Vehicle(s) Owned</h5>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm small">
                            <thead class="bg-dark-blue">
                                <tr>
                                    <th>Category</th>
                                    <th>Make</th>
                                    <th>Model</th>
                                    <th>Yeah</th>
                                    <th>Registration Number</th>
                                    <th>Avg. Rating</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($vehicles as $vehicle) { ?>
                                    <tr>
                                        <th><?php echo $vehicle['category'] ?></th>
                                        <th><?php echo $vehicle['make'] ?></th>
                                        <th><?php echo $vehicle['model']?></th>
                                        <th><?php echo $vehicle['year']?></th>
                                        <th><?php echo $vehicle['registration_number'] ?></th>
                                        <th><?php echo round($vehicle['average_rating'],2) ?></th>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <h5 class="font-weight-bold">Trips</h5>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm small">
                            <thead class="bg-dark-blue">
                                <tr>
                                    <th>Vehicle ID</th>
                                    <th>Seat Number</th>
                                    <th>Driver</th>
                                    <th>Start Time</th>
                                    <th>Start Location</th>
                                    <th>End Time</th>
                                    <th>End Location </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($trips as $trip) { ?>
                                    <tr>
                                        <th><?php echo $trip['registration_number'] ?></th>
                                        <th><?php echo $trip['seat_number'] ?></th>
                                        <th><?php echo $trip['first_name'] . " " . $trip['last_name'] ?></th>
                                        <th><?php echo $trip['start_time'] ?></th>
                                        <th><?php echo $trip['start_location'] ?></th>
                                        <th><?php echo $trip['end_time'] ?></th>
                                        <th><?php echo $trip['end_location'] ?></th>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>


                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
                </div>
            </div>
        </div>
    </div>
</div>
</div>
