
<?php

//This file is used to process request for registered people 
//The request is registered and request parameters are used 
//to refine/filter the searches

require '../_includes/connect.php';

$result = array();
$result['success'] = false;
$firstName = $lastName = $street = $town = $postalCode = $gender = $dob1 = $dob2 = $regId = "";

//The result is paginated with 20 records by page. if not page number is 
//supplied, page is assumed to be one.
if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

if (isset($_GET['size'])) {
    $recordsPerPage = intval($_GET['size']);
} else {
    $recordsPerPage = 20;
}


$offset = ($page - 1) * $recordsPerPage;

//Sql to count total result to be filed.
$sqlCount = "SELECT COUNT(*) count FROM people ";

//Sql to feltch result.
$sqlFetch = "SELECT * FROM people";
$bindArray = array();

//Get search request parameters
if (isset($_POST['first_name'])) {
    $firstName = cleanInput($_POST["first_name"]);
}
if (isset($_POST['last_name'])) {
    $lastName = cleanInput($_POST["last_name"]);
}
if (isset($_POST['town'])) {
    $town = cleanInput($_POST["town"]);
}
if (isset($_POST['postal_code'])) {
    $postalCode = cleanInput($_POST["postal_code"]);
}
if (isset($_POST['gender'])) {
    $gender = cleanInput($_POST["gender"]);
}
if (isset($_POST['street'])) {
    $street = cleanInput($_POST["street"]);
}
if (isset($_POST['date1'])) {
    $dob1 = cleanInput($_POST["date1"]);
}
if (isset($_POST['date2'])) {
    $dob2 = cleanInput($_POST["date2"]);
}
if (isset($_POST['reg_id'])) {
    $regId = cleanInput($_POST["reg_id"]);
}

//Filter result based on search parameters
if (!empty($firstName)) {
    $sql = endsWith($sqlCount, "?") ? " AND first_name LIKE ?" : " WHERE first_name LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $firstName);
}

if (!empty($lastName)) {
    $sql = endsWith($sqlCount, "?") ? " AND last_name LIKE ?" : " WHERE last_name LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $lastName);
}

if (!empty($town)) {
    $sql = endsWith($sqlCount, "?") ? " AND address_town = ?" : " WHERE address_town = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $town);
}


if (!empty($postalCode)) {
    $sql = endsWith($sqlCount, "?") ? " AND address_post_code = ?" : " WHERE address_post_code = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $postalCode);
}

if (!empty($gender)) {
    $sql = endsWith($sqlCount, "?") ? " AND gender = ?" : " WHERE gender = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $gender);
}

if (!empty($street)) {
    $sql = endsWith($sqlCount, "?") ? " AND address_street = ?" : " WHERE address_street = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $street);
}

if (!empty($regId)) {
    $sql = endsWith($sqlCount, "?") ? " AND registration_id = ?" : " WHERE registration_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $regId);
}

if (!empty($dob1)) {
    $sql = endsWith($sqlCount, "?") ? " AND birth_date >= ?" : " WHERE birth_date >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $dob1);
}

if (!empty($dob2)) {
    $sql = endsWith($sqlCount, "?") ? " AND birth_date <= ?" : " WHERE birth_date <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $dob2);
}

$stmt = $conn->prepare($sqlCount);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

//Execute statement
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

$totalRows = $stmt->get_result()->fetch_assoc()['count'];
$totalPages = ceil($totalRows / $recordsPerPage);

$sqlFetch = $sqlFetch . " LIMIT $offset, $recordsPerPage";

$stmt = $conn->prepare($sqlFetch);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

// Return Json result
$result['success'] = true;
$result['data'] = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
$result['count'] = count($result['data']);
$result['total'] = $totalRows;
$result['from'] = $offset + 1;
$result['to'] = $offset + $result['count'];
$result['currentPage'] = $page;
$result['totalPages'] = $totalPages;
die(json_encode($result, JSON_NUMERIC_CHECK));