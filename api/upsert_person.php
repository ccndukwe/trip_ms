
<?php

//This file is used to add new person or update existing person.
//if the id is null, new record is added to the database, otherwise update is done.


require '../_includes/connect.php';

$firstName = $lastName = $street = $town = $postalCode = $number = $email = $gender = $dob = $regId = "";

$result = array();
$result['success'] = false;

//Verify that request is POST
if ($_SERVER["REQUEST_METHOD"] === "POST") {

      //Get request Parameters
    $id = cleanInput($_POST["selected_id"]);
    $firstName = cleanInput($_POST["first_name"]);
    $lastName = cleanInput($_POST["last_name"]);
    $street = cleanInput($_POST["street"]);
    $number = cleanInput($_POST["number"]);
    $town = cleanInput($_POST["town"]);
    $postalCode = cleanInput($_POST["postal_code"]);
    $email = cleanInput($_POST["email"]);
    $gender = cleanInput($_POST["gender"]);
    $dob = cleanInput($_POST["birth_date"]);
    $regId = cleanInput($_POST["reg_id"]);

     // Validate required inputs
    if (empty($firstName) || empty($lastName) || empty($street) ||
            empty($number) || empty($town) || empty($postalCode) ||
            empty($email) || empty($regId) || empty($dob)) {

        $result['status'] = false;
        $result['message'] = "Some form input is missing, please review and try again";
        die(json_encode($result));
    }

       // If ID is null perform insert else perform update
    if (empty($id)) {
        $stmt = $conn->prepare("INSERT INTO people "
                . "(first_name, last_name, address_street, address_number,"
                . " address_town, address_post_code, birth_date, email,"
                . " gender, registration_id) VALUES (?, ?, ?, ?, ?, ?,?, ?, ?,?)");

        $stmt->bind_param("ssssssssss", $firstName, $lastName, $street, $number, $town,
                $postalCode, $dob, $email, $gender, $regId);
        if (!$stmt->execute()) {
            $result['success'] = false;
            $result['message'] = $stmt->error;
            die(json_encode($result));
        }
        $last_id = $conn->insert_id;
    } else {

        $stmt = $conn->prepare("UPDATE people SET first_name = ?, last_name = ?, address_street = ?, address_number = ?,"
                . " address_town = ?, address_post_code = ?, birth_date = ?, email = ?,"
                . " gender = ?, registration_id= ? WHERE id = ?");

        $stmt->bind_param("sssssssssss", $firstName, $lastName, $street, $number, $town,
                $postalCode, $dob, $email, $gender, $regId, $id);

        if (!$stmt->execute()) {
            $result['success'] = false;
            $result['message'] = $stmt->error;
            die(json_encode($result));
        }
        $last_id = $id;
    }



 // upload the picture and add the file name to the object in the DB
    if (!empty($_FILES["picture"]) && !empty($_FILES["picture"]["tmp_name"])) {

        $target_dir = "../uploads/";
        $fileName = time() . basename($_FILES["picture"]["name"]);
        $target_file = $target_dir . $fileName;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["picture"]["tmp_name"]);
        if ($check !== false) {
            move_uploaded_file($_FILES["picture"]["tmp_name"], $target_file);

            $stmt = $conn->prepare("UPDATE people SET picture = ? WHERE id = ?");
            $stmt->bind_param("ss", $fileName, $last_id);
            $stmt->execute();
        }
    }

    // Return Json response
    $result['success'] = true;
    $result['message'] = "Operation successfully completed";
    die(json_encode($result));
}
        