
<?php

//This file is used to process request for incident types 
//The request parameters are used to refine/filter the searches.

require '../_includes/connect.php';

$result = array();
$result['success'] = false;
$name = $description = "";

//The result is paginated with 20 records by page. if not page number is 
//supplied, page is assumed to be one.
if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

if (isset($_GET['size'])) {
    $recordsPerPage = intval($_GET['size']);
} else {
    $recordsPerPage = 20;
}


$offset = ($page - 1) * $recordsPerPage;

//Sql to count total result to be filed.
$sqlCount = "SELECT COUNT(*) count FROM incident_types ";

//Sql to feltch result.
$sqlFetch = "SELECT * FROM incident_types";
$bindArray = array();

if (isset($_POST['name'])) {
    $name = cleanInput($_POST["name"]);
}
if (isset($_POST['class'])) {
    $description = cleanInput($_POST["description"]);
}

//Filter result based on search inputs
if (!empty($name)) {
    $sql = endsWith($sqlCount, "?") ? " AND name LIKE ?" : " WHERE name LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $name);
}

if (!empty($description)) {
    $sql = endsWith($sqlCount, "?") ? " AND description LIKE ?" : " WHERE description LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $description);
}

$stmt = $conn->prepare($sqlCount);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

//Execute statement
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

$totalRows = $stmt->get_result()->fetch_assoc()['count'];
$totalPages = ceil($totalRows / $recordsPerPage);

$sqlFetch = $sqlFetch . " LIMIT $offset, $recordsPerPage";

$stmt = $conn->prepare($sqlFetch);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

//Execute statement
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

// Return Json result
$result['success'] = true;
$result['data'] = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
$result['count'] = count($result['data']);
$result['total'] = $totalRows;
$result['from'] = $offset + 1;
$result['to'] = $offset + $result['count'];
$result['currentPage'] = $page;
$result['totalPages'] = $totalPages;
die(json_encode($result, JSON_NUMERIC_CHECK));
