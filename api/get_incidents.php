
<?php

//This file is used to process request for incidents 
//The request parameters are used to refine/filter the searches.

require '../_includes/connect.php';

$result = array();
$result['success'] = false;
$roadId = $tripId = $remark = $typeId = $date1 = $date2  = "";

//The result is paginated with 20 records by page. if not page number is 
//supplied, page is assumed to be one.
if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

if (isset($_GET['size'])) {
    $recordsPerPage = intval($_GET['size']);
} else {
    $recordsPerPage = 20;
}


$offset = ($page - 1) * $recordsPerPage;

//Sql to count total result to be filed.
$sqlCount = "SELECT COUNT(*) count FROM incidents i inner join roads r on i.road_id = r.id "
        . "inner join trips t on i.trip_id = t.id inner join vehicles v on t.vehicle_id = v.id "
        . "inner join incident_types it on i.incident_type_id = it.id "
        . "inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id ";

//Sql to feltch result.
$sqlFetch = "SELECT i.*, r.name as roadName,it.name as incidentName, v.registration_number, "
        . "t.start_location, t.start_time, pp.first_name, pp.last_name FROM incidents i "
        . "inner join roads r on i.road_id = r.id inner join trips t on i.trip_id = t.id "
        . "inner join incident_types it on i.incident_type_id = it.id "
        . "inner join drivers d on t.driver_id = d.id inner join people pp on d.person_id = pp.id "
        . "inner join vehicles v on t.vehicle_id = v.id ";

$bindArray = array();

//Get search request parameters
if (isset($_POST['remark'])) {
    $remark = cleanInput($_POST["remark"]);
}
if (isset($_POST['date1'])) {
    $date1 = cleanInput($_POST["date1"]);
}
if (isset($_POST['date2'])) {
    $date2 = cleanInput($_POST["date2"]);
}
if (isset($_POST['road_id'])) {
    $roadId = cleanInput($_POST["road_id"]);
}
if (isset($_POST['trip_id'])) {
    $tripId = cleanInput($_POST["trip_id"]);
}
if (isset($_POST['incident_type_id'])) {
    $typeId = cleanInput($_POST["incident_type_id"]);
}

//Filter result based on search parameters
if (!empty($remark)) {
    $sql = endsWith($sqlCount, "?") ? " AND i.remark  LIKE ?" : " WHERE i.remark LIKE ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $remark);
}

if (!empty($roadId)) {
    $sql = endsWith($sqlCount, "?") ? " AND i.road_id = ?" : " WHERE i.road_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $roadId);
}

if (!empty($tripId)) {
    $sql = endsWith($sqlCount, "?") ? " AND i.trip_id = ?" : " WHERE i.trip_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $tripId);
}

if (!empty($typeId)) {
    $sql = endsWith($sqlCount, "?") ? " AND i.incident_type_id = ?" : " WHERE i.incident_type_id = ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $typeId);
}


if (!empty($date1)) {
    $sql = endsWith($sqlCount, "?") ? " AND i.incident_time >= ?" : " WHERE i.incident_time >= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $date1);
}

if (!empty($date2)) {
    $sql = endsWith($sqlCount, "?") ? " AND i.incident_time <= ?" : " WHERE i.incident_time <= ?";
    $sqlCount = $sqlCount . $sql;
    $sqlFetch = $sqlFetch . $sql;
    array_push($bindArray, $date2);
}

$stmt = $conn->prepare($sqlCount);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}

//Execute statement
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

$totalRows = $stmt->get_result()->fetch_assoc()['count'];
$totalPages = ceil($totalRows / $recordsPerPage);

$sqlFetch = $sqlFetch . " LIMIT $offset, $recordsPerPage";

$stmt = $conn->prepare($sqlFetch);
if (!empty($bindArray)) {
    $stmt->bind_param(str_repeat('s', count($bindArray)), ...$bindArray);
}
//die($sqlFetch);
if (!$stmt->execute()) {
    $result['success'] = false;
    $result['message'] = $stmt->error;
    die(json_encode($result));
}

// Return Json result
$result['success'] = true;
$result['data'] = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
$result['count'] = count($result['data']);
$result['total'] = $totalRows;
$result['from'] = $offset + 1;
$result['to'] = $offset + $result['count'];
$result['currentPage'] = $page;
$result['totalPages'] = $totalPages;
die(json_encode($result, JSON_NUMERIC_CHECK));
