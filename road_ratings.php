<!DOCTYPE html>
<html lang="en">

    <?php
    
    //    This file is used to display the list of road ratings.
    
    $currentLink = "road_ratings";
    $pageTitle = "Road Ratings";
    ?>
    <?php require './_includes/header.php'; ?>

    <body ng-app="tripApp" id="page-top" ng-controller="appCtrl" >

        <!-- Page Wrapper -->
        <div id="wrapper">

            <?php require './_includes/siderbar.php'; ?>

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <?php require './_includes/topbar.php'; ?>

                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Content Row -->
                        <div class="row">
                            <div class="col-12 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <span>Search</span>

                                        <form method="post" class="form"  name="searchForm" id="searchForm">

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="rating11">Rating from</label>
                                                    <select id="rating11" name="rating1" class="form-control"> 
                                                        <option value="" selected>Choose...</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="rating131">Rating to</label>
                                                    <select id="rating131" name="rating2" class="form-control"> 
                                                        <option value="" selected>Choose...</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="roads1">Road</label>
                                                    <select id="roads1" name="road_id" class="form-control m-1"> 
                                                        <option value="" selected>Choose...</option>
                                                        <option ng-repeat="item in roads" ng-value="{{item.id}}">
                                                            {{item.name}}
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputpeople11">Person</label>
                                                    <select id="inputpeople11" name="person_id" class="form-control"> 
                                                        <option value="" selected>Choose...</option>
                                                        <option ng-repeat="item in people" ng-value="{{item.id}}">
                                                            {{item.first_name + " " + item.last_name}}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <button type="submit"
                                                    class="btn btn-primary m-2 float-right">  <i ng-show="processing" class="fa fa-spinner fa-spin"></i> Search</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Content Row -->

                        <div class="row">

                            <div class="col-12">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div
                                        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Road Ratings 
                                            <i class="small text-black font-weight-lighter" ng-show="result.total">
                                                [Showing {{result.from}} - {{result.to}} of {{result.total}}
                                                result{{result.total>1?'s':''}}]</i>
                                            <i ng-show="!result.total" class="small text-black font-weight-lighter">[No result found]</i>
                                            <i>&nbsp;&nbsp;</i>
                                        </h6>
                                        <div class="dropdown no-arrow">
                                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                                 aria-labelledby="dropdownMenuLink">
                                                <div class="dropdown-header">Actions</div>
                                                <a class="dropdown-item" href="#" ng-click="showUpsertModal(null)" >Add Comment</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body pt-0">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-sm small">
                                                <thead class="bg-dark-blue">
                                                    <tr>
                                                        <th class="text-center">Rating</th>
                                                        <th>Road</th>
                                                        <th>Person</th>
                                                        <th>Comment</th>
                                                        <th class="text-center">Comment Date</th>
                                                        <th class="text-right pr-2">
                                                            Actions
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="each in result.data| orderBy: propertyName: reverse track by $index">
                                                        <td class="text-center">{{each.rating}}</td>
                                                        <td> {{each.roadName}}</td>
                                                        <td>{{each.first_name + " " + each.last_name}}</td>
                                                        <td>{{each.rating_comment}}</td>
                                                        <td class="text-center">{{each.created_at| date: 'dd MMM yyyy hh:mm a'}}</td>
                                                        <td class="text-center">
                                                            <span class="dropdown pull-right pt-1">
                                                                <button data-toggle="dropdown"
                                                                        class="p-0 btn btn-light dropdown-toggle btn-sm text-dark-blue comment_icon_action pr-1 pl-1 mr-1 ml-1"></button>
                                                                <div class="dropdown-menu rounded pl-3 pr-3">
                                                                    <li class="small"> <a
                                                                            ng-click="showUpsertModal(each)"
                                                                            href="#">Modify</a></li>
                                                                    <div class="dropdown-divider"></div>
                                                                    <li class="small"> 
                                                                        <a class="text-red"
                                                                           ng-click="showDeleteModal(each)"
                                                                           href="#">Delete</a></li>
                                                                    <div class="dropdown-divider"></div>
                                                                </div>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row" ng-show="result.totalPages > 1">
                                            <div class="col clearfix">
                                                <button ng-show="result.currentPage > 1" type="button"
                                                        ng-click="getPrevious()"
                                                        class="btn btn-light btn-sm float-left">
                                                    <i class="fa fa-arrow-left"></i>&nbsp;Previous
                                                </button>
                                            </div>
                                            <div class="col clearfix">
                                                <button ng-show="result.currentPage < result.totalPages" type="button"
                                                        ng-click="getNext()"
                                                        class="btn btn-light btn-sm float-right">
                                                    Next&nbsp; <i class="fa fa-arrow-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /.container-fluid -->

                    </div>
                    <!-- End of Main Content -->

                    <!-- Footer -->
                        <?php require './_includes/sticky_footer.php'; ?>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
        </div>
        <!-- End of Page Wrapper -->
        <script src="js/road_ratings.js"></script>
        <?php require './_includes/modals/upsert_road_rating.php'; ?>
        <?php require './_includes/modals/delete_road_rating.php'; ?>
        <?php require './_includes/footer.php'; ?>
    </div>
</body>

</html>